/// <reference path="../../typings/tsd.d.ts" />
/// <reference path="model/model.ts" />
(function(){
	var app = angular.module('Services', []);

	app.factory('FantaCalcioServices', ['$q','$http', '$rootScope', function($q, $http, $rootScope){
		
	$rootScope.torneoClassicoUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
	//TUTTO SPORT LEAGUE CLASSICO
		?'http://localhost:9292/league.tsport.yland.it/'
		// :'http://league.tsport.yland.it/';     					//ESTERNA 
		:'http://league.tuttosport.com/';							//PRODUZIONE 
		//:'http://devts.tuttosport.com/';       						//RETE AZIENDALE Y TECH
	//MISTER CALCIO CUP CLASSICO
		//?'http://localhost:9292/gp.mr.yland.it/'
		//:'http://gp.mr.yland.it/'									//RETE ESTERNA
		//:'http://devgp.corrieredellosport.it/'                       	//RETE AZIENDALE Y TECH
		;

	$rootScope.torneoGironiUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
	//TUTTO SPORT LEAGUE GIRONI modo_gioco = 1
		?'http://localhost:9292/gironi.tsport.yland.it/'
		// :'http://gironi.tsport.yland.it/';       					//ESTERNA
		:'http://gironi-league.tuttosport.com/';				//PRODUZIONE
		//:'http://devtsgironi.tuttosport.com/';       				//RETE AZIENDALE Y TECH
	//MISTER CALCIO CUP GIRONI modo_gioco = 1
		//?'http://localhost:9292/www.mr.yland.it/'
		//:'http://www.mr.yland.it/';								//ESTERNA
		//:'http://devmcc.corrieredellosport.it/'                       	//RETE AZIENDALE Y TECH
		;

	$rootScope.torneoLegheUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
	//TUTTO SPORT LEAGUE LEGHE modo_gioco = 2
		?'http://localhost:9292/gironi.tsport.yland.it/'
		// :'http://gironi.tsport.yland.it/';       							//ESTERNA
		:'http://gironi-league.tuttosport.com/';					//PRODUZIONE
		//:'http://devtsgironi.tuttosport.com/';       				//RETE AZIENDALE Y TECH
	//MISTER CALCIO CUP LEGHE modo_gioco = 1  
		//?'http://localhost:9292/www.mr.yland.it/'
		//:'http://www.mr.yland.it/';								//ESTERNA
		//:'http://devmcc.corrieredellosport.it/'					//RETE AZIENDALE Y TECH
		;

	$rootScope.torneoPlayoffUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
	//TUTTO SPORT LEAGUE PLAYOFF modo_gioco = 4
		?'http://localhost:9292/gironi.tsport.yland.it/'
		// :'http://gironi.tsport.yland.it/';       							//ESTERNA
		:'http://gironi-league.tuttosport.com/';					//PRODUZIONE
		//:'http://devtsgironi.tuttosport.com/';       				//RETE AZIENDALE Y TECH
	//MISTER CALCIO CUP PLAYOFF modo_gioco = 1
		//?'http://localhost:9292/www.mr.yland.it/'
		//:'http://www.mr.yland.it/';								//ESTERNA
		//:'http://devmcc.corrieredellosport.it/'					//RETE AZIENDALE Y TECH
		;

		return {
			authentication: function(username: string, password: string, modoGioco: any, deviceToken: string, deviceId: string, deviceTarget: string){ // /authentication.json?action=login&appversion=1.3.4&modo_gioco=3&os=ios&password=ardizzone&tipologin=sso&username=lorenzo.ardizzone@gmail.com
				var urlServizi;
				//var modo = parseInt($rootScope.modoGioco);
				switch(parseInt(modoGioco)){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var os;
				if (typeof deviceTarget == 'undefined') {
					os = 'browser';
					deviceId = null;
					deviceTarget = null;
					deviceToken = null;
				}
				else {
					os = deviceTarget.toLowerCase();
					if (os == 'apple') {
						os = 'ios';
					}
				}
				console.log("authentication urlServizi: " + urlServizi
					+ " username: " + username
					+ " deviceId: " + deviceId
					+ " deviceToken: " + deviceToken
					+ " deviceTarget: " + deviceTarget
					+ " os: " + os);
				var deferred = $q.defer();
				$http.get(urlServizi+'mobile/authentication.json', { params: { 
					os: 'ios', 
					appversion: '1.0', 
					tipologin: 'sso',
					password: password,
					action: 'login',
					modo_gioco: modoGioco,
					username: username,
					deviceId: deviceId,
					deviceToken: deviceToken,
					deviceTarget: deviceTarget
					} }).success(function (data : OM.AuthenticationResponse) {
						deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data);
				});
				return deferred.promise;
			},
			facebookAuthentication: function(username: string, password: string, modoGioco: any, token: string, deviceToken: string, deviceId: string, deviceTarget: string){
		    // /authentication.json?action=login&appversion=1.3.4&modo_gioco=3&os=ios&password=ardizzone&tipologin=sso&username=lorenzo.ardizzone@gmail.com
			//facebookAuthentication: function(modoGioco: any, token: string){ // /authentication.json?action=login&appversion=1.3.4&modo_gioco=3&os=ios&password=ardizzone&tipologin=sso&username=lorenzo.ardizzone@gmail.com
				var urlServizi;
				//var modo = parseInt($rootScope.modoGioco);
				switch(parseInt(modoGioco)){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var os;
				if (typeof deviceTarget == 'undefined') {
					os = 'browser';
					deviceId = null;
					deviceTarget = null;
					deviceToken = null;
				}
				else {
					os = deviceTarget.toLowerCase();
					if (os == 'apple') {
						os = 'ios';
					}
				}
				console.log("fb authentication urlServizi: " + urlServizi
					+ " username: " + username
					+ " deviceId: " + deviceId
					+ " deviceToken: " + deviceToken
					+ " deviceTarget: " + deviceTarget
					+ " os: " + os);
				var deferred = $q.defer();
				$http.get(urlServizi+'mobile/authentication.json', { params: { 
					os: 'ios', 
					appversion: '1.0', 
					tipologin: 'facebook',
					password: password,
					action: 'login',
					token: token,
					modo_gioco: modoGioco,
					username: username,
					deviceId: deviceId,
					deviceToken: deviceToken,
					deviceTarget: deviceTarget
					} }).success(function (data : OM.AuthenticationResponse) {
						deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data);
				});
				return deferred.promise;
			},
			logout: function(idCliente:number, modoGioco: number){ // /mobile/mobile/login.json?action=logout&ic=0&modo_gioco=3&id_cliente=0
				var urlServizi;
				var modo = parseInt($rootScope.modoGioco);
				switch(modo){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var deferred = $q.defer();
				$http.get(urlServizi+'mobile/login.json', {
					params: {
						action: 'logout',
						ic: idCliente,
						modo_gioco: modoGioco,
					}
					}).success(function(data: OM.LogoutResponse){
							deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			registraAPlayoff: function(idSquadra: number){
				var urlServizi;
				//var modo = parseInt($rootScope.modoGioco);
				switch(parseInt($rootScope.modoGioco)){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var deferred = $q.defer();
				$http.get(urlServizi+'/mobile/playoff.json', {
					params: {
						is: idSquadra,
						action: 'IscriviSquadra'
					}
					}).success(function(data: OM.GetSquadreResponse){
						deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			getSquadre: function(idCliente: number, modoGioco: any){ // /mobile/home.json?action=getSquadre&modo_gioco=3&ic=1000002
				var urlServizi;
				//var modo = parseInt($rootScope.modoGioco);
				switch(parseInt(modoGioco)){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var deferred = $q.defer();
				$http.get(urlServizi+'mobile/home.json', {
					params: {
						ic: idCliente,
						action: 'getSquadre',
						modo_gioco: modoGioco
					}
					}).success(function(data: OM.GetSquadreResponse){
							deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			getCalendario: function(idSquadra: number){ // /mobile/Calendario.json?is=1000002&action=getCalendario
				var urlServizi;
				var modo = parseInt($rootScope.modoGioco);
				switch(modo){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var deferred = $q.defer();
				$http.get(urlServizi+'mobile/Calendario.json', {
					params: {
						is: idSquadra,
						action: 'getCalendario'
					}
					}).success(function(data: OM.GetCalendarioResponse){
						deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},            
			getDettaglioGiornata: function(idSquadra: number, idPartita: number){ // /mobile/tabellino.json?ip=1&is=1000002&action=getDettaglio
				var urlServizi;
				var modo = parseInt($rootScope.modoGioco);
				switch(modo){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var deferred = $q.defer();
				$http.get(urlServizi+'mobile/tabellino.json', {
					params: {
						ip: idPartita,
						is: idSquadra,
						action: 'getDettaglio'
					}
					}).success(function(data: OM.GetDettaglioGiornataResponse){
						deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			getClassifica: function(idSquadra: number){ // /mobile/classifica.json?is=1000002&action=getClassifica
				var urlServizi;
				var modo = parseInt($rootScope.modoGioco);
				switch(modo){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var deferred = $q.defer();
				$http.get(urlServizi+'mobile/classifica.json', {
					params: {
						is: idSquadra,
						action: 'getClassifica'
					}
					}).success(function(data: OM.GetClassificaResponse){
							deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			modificaAvatarSquadra: function(idCampionato: number, idSquadra: number){   // /mobile/squadra.json?action=addSquadra&idc=1000001&operation=upd&modo_gioco=3&avatarlist=true&is=1000002
				var urlServizi;
				var modo = parseInt($rootScope.modoGioco);
				switch(modo){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var deferred = $q.defer();                                              //stemma.json?action=getStemmi  ????
				$http.get(urlServizi+'mobile/stemma.json', {
					params: {
						action: 'getStemmi',
						//idc: idCampionato, //idCampionato,
						//operation: 'upd',
						//modo_gioco: 3,
						//avatarlist: true,
						//is: idSquadra
					}
					}).success(function(data: OM.ModificaAvatarSquadraResponse){
						deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			modificaSquadra: function(idCampionato: number, idSquadra: number, nomeSquadra: string, idAvatar: number){ // /mobile/squadra.json?team=Squadra_1000002&idavatar=1&action=modificaSquadra&is=1000002
				var urlServizi;
				var modo = parseInt($rootScope.modoGioco);
				switch(modo){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var deferred = $q.defer();
				$http.get(urlServizi+'mobile/squadra.json', {
					params: {
						team: nomeSquadra,
						idavatar: idAvatar,
						action: 'modificaSquadra',
						is: idSquadra
					}
					}).success(function(data: OM.ModificaSquadraResponse){
						deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			eliminaSquadra: function(idSquadra: number){ // /mobile/squadra.json?action=cancellaSquadra&is=1000002
				var urlServizi;
				var modo = parseInt($rootScope.modoGioco);
				switch(modo){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var deferred = $q.defer();
				$http.get(urlServizi+'mobile/squadra.json', {
					params: {
						action: 'cancellaSquadra',
						is: idSquadra
					}
				}).success(function(data: OM.NuovaSquadraResponse){
					deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			nuovaSquadra: function(idCliente: number, modoGioco: any){ // /mobile/squadra.json?action=addSquadra&idc=1000001&operation=add&modo_gioco=3&avatarlist=false
				var urlServizi;
				//var modo = parseInt($rootScope.modoGioco);
				switch(parseInt(modoGioco)){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var deferred = $q.defer();
				$http.get(urlServizi+'mobile/squadra.json', {
					params: {
						action: 'addSquadra',
						idc: idCliente,
						operation: 'add',
						modo_gioco: modoGioco,
						avatarlist: false
					}
					}).success(function(data: OM.NuovaSquadraResponse){
						deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			gestioneRosa: function(idCampionato: number, idSquadra: number){ // /scripts/rosadata.jsp?is=1000002&ic=3&format=json
				var urlServizi;
				var modo = parseInt($rootScope.modoGioco);
				switch(modo){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var deferred = $q.defer();
				$http.get(urlServizi+'scripts/rosadata.jsp', {
					params: {
						is: idSquadra,
						ic: idCampionato,
						format: 'json'
					}
					}).success(function(data: OM.GestioneRosaResponse){
						deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			modificaRosa: function(idSquadra: number, sold: string, bought: string){ // /gprixteam/create_rosa_mobile.jsp?rand=1957747793&sold=101&ic=3&bought=473%3A5&is=1000002&format=json
				var urlServizi;
				var modo = parseInt($rootScope.modoGioco);
				switch(modo){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var date = new Date();
				var rand = date.getTime();
				var deferred = $q.defer();
				var append = '';
				if(modo == 3)
					append = 'gprixteam/create_rosa_mobile.jsp'
				else
					append = 'squadra/create_rosa_mobile.jsp'
				$http.get(urlServizi+append, {
					params: {
						rand: rand,
						sold: sold,
						bought: bought,
						is: idSquadra,
						format: 'json'
					}
					}).success(function(data: OM.SuccessModificaRosaResponse){
						deferred.resolve(data);
				}).error(function(data: OM.ErrorModificaRosaResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			formazioneData: function(idCampionato: number, idSquadra: number){ // /scripts/formazionedata.jsp?is=1000002&ic=3&format=json
				var urlServizi;
				var modo = parseInt($rootScope.modoGioco);
				switch(modo){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var deferred = $q.defer();
				$http.get(urlServizi+'scripts/formazionedata.jsp', {
					params: {
						is: idSquadra,
						ic: idCampionato,
						format: 'json'
					}
					}).success(function(data: OM.FormazioneDataResponse){
						deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			salvaFormazione: function(idCampionato: number, idSquadra: number, tattica: number, formazione: string){ // /gprixteam/create_formazione_mobile.jsp?rand=424238335&tattica=1&ic=3&add=203%3A0%3B281%3A1%3B657%3A2%3B208%3A3%3B170%3A4%3B473%3A5%3B218%3A6%3B50%3A7%3B296%3A8%3B302%3A9%3B268%3A10%3B430%3A11%3B278%3A12%3B246%3A13%3B316%3A14%3B384%3A15%3B425%3A16%3B148%3A17&is=1000002&format=json
				var urlServizi;
				var modo = parseInt($rootScope.modoGioco);
				switch(modo){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var date = new Date();
				var rand = date.getTime();
				var deferred = $q.defer();
				var urlChiamata = '';
				if(modo == 3){
					urlChiamata = 'gprixteam/create_formazione_mobile.jsp';
				} else {
					urlChiamata = 'squadra/create_formazione_mobile.jsp';
				}
				$http.get(urlServizi + urlChiamata, {
					params: {
						rand: rand,
						tattica: tattica,
						ic: idCampionato,
						add: formazione,
						is: idSquadra,
						format: 'json'
					}
					}).success(function(data: OM.SalvaFormazioneResponse){
						deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			modificaFormazione: function(idCampionato: number, idSquadra: number, tattica: number, remove: string, add: string){ // non c'è
				var urlServizi;
				var modo = parseInt($rootScope.modoGioco);
				switch(modo){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var date = new Date();
				var rand = date.getTime();
				var deferred = $q.defer();
				var urlChiamata = '';
				if(modo == 3){
					urlChiamata = 'gprixteam/create_formazione_mobile.jsp';
				} else {
					urlChiamata = 'squadra/create_formazione_mobile.jsp';
				}
				$http.get(urlServizi+urlChiamata, {
					params: {
						ic: idCampionato,
						is: idSquadra,
						tattica: tattica,
						remove: remove,
						add: add,
						rand: rand
					}
				}).success(function(data: OM.SalvaFormazioneResponse){
						deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			}, 
			cancellaFormazione: function(idCampionato: number, idSquadra: number, tattica: number){ // /gprixteam/create_formazione_mobile.jsp?rand=719885386&tattica=1&ic=3&clrfrmzn=yes&is=1000002
				var urlServizi;
				var modo = parseInt($rootScope.modoGioco);
				switch(modo){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var date = new Date();
				var rand = date.getTime();
				var deferred = $q.defer();
				var urlChiamata = '';
				if(modo == 3){
					urlChiamata = 'gprixteam/create_formazione_mobile.jsp';
				} else {
					urlChiamata = 'squadra/create_formazione_mobile.jsp';
				}
				$http.get(urlServizi+urlChiamata, {
					params: {
						rand: rand,
						tattica: tattica,
						ic: idCampionato,
						clrfrmzn: 'yes',
						is: idSquadra,
						format: 'json'
					}
				}).success(function(data: OM.CancellaFormazioneResponse){
						deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			getNews: function(){ // http://<SITO_GIOCO_CLASSICO>/mobile/news.json?action=getNews
				var urlServizi;
				urlServizi = $rootScope.torneoClassicoUrl;
				var deferred = $q.defer();
				$http.get(urlServizi+'mobile/news.json', {
					params: {
						action: "getNews"
					}
				}).success(function(data: OM.CancellaFormazioneResponse){
					deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			registraUtente: function(email: string, password: string, nome: string, cognome: string, username: string, dataNascita: string, 
				regolamento: number, fl_privacy: number, fl_mktg: number, fl_servizio_mcc: number, fl_servizio_cds: number, 
				fl_articoli_privacy: number, modoGioco: any){ // http://league.tsport.yland.it/mobile/register.json?USERNAME=pippofake&fl_privacy=1&fl_mktg=0&Regolamento=1&fl_servizio_mcc=1&fl_servizio_cds=1&fl_articoli_privacy=1&action=addUser&modo_gioco=3&MAIL=pippofake%40fake.it&DTNASCITA=21%2F04%2F1990&COGNOME=manneschi&NOME=pietro&PASSWORD=pippofake1
				var urlServizi;
				//var modo = parseInt($rootScope.modoGioco);
				switch(parseInt(modoGioco)){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var deferred = $q.defer();
				$http.get(urlServizi+'mobile/register.json', {
					params: {
						action: 'addUser',
						MAIL: email,
						PASSWORD: password,
						NOME: nome,
						COGNOME: cognome,
						USERNAME: username,
						DTNASCITA: dataNascita,
						Regolamento: regolamento,
						fl_privacy: fl_privacy,
						fl_mktg: fl_mktg,
						fl_servizio_mcc: fl_servizio_mcc,
						fl_servizio_cds: fl_servizio_cds,
						fl_articoli_privacy: fl_articoli_privacy,
						modo_gioco: modoGioco
					}
				}).success(function(data: OM.CancellaFormazioneResponse){
					deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			registraUtenteFB: function(email: string, token: string, regolamento: number, fl_privacy: number, fl_mktg: number, fl_servizio_mcc: number, fl_servizio_cds: number, 
				fl_articoli_privacy: number, modoGioco: any){ // http://league.tsport.yland.it/mobile/register.json?USERNAME=pippofake&fl_privacy=1&fl_mktg=0&Regolamento=1&fl_servizio_mcc=1&fl_servizio_cds=1&fl_articoli_privacy=1&action=addUser&modo_gioco=3&MAIL=pippofake%40fake.it&DTNASCITA=21%2F04%2F1990&COGNOME=manneschi&NOME=pietro&PASSWORD=pippofake1
				var urlServizi;
				//var modo = parseInt($rootScope.modoGioco);
				switch(parseInt(modoGioco)){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var deferred = $q.defer();
				$http.get(urlServizi+'mobile/registerFB.json', {
					params: {
						email:email,
						action: 'addUser',
						Regolamento: regolamento,
						fl_privacy: fl_privacy,
						fl_mktg: fl_mktg,
						fl_servizio_mcc: fl_servizio_mcc,
						fl_servizio_cds: fl_servizio_cds,
						fl_articoli_privacy: fl_articoli_privacy,
						modo_gioco: modoGioco,
						token: token
					}
				}).success(function(data: OM.CancellaFormazioneResponse){
					deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			getProfilo: function(idCliente: number){  // /mobile/user.json?action=datiProfilo&idc=1000002
				var urlServizi;
				var modo = parseInt($rootScope.modoGioco);
				switch(modo){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var deferred = $q.defer();
				$http.get(urlServizi+'mobile/user.json', {
					params: {
						action: 'datiProfilo',
						idc: idCliente
					}
				}).success(function(data: OM.GetProfiloResponse){
					deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			},
			modificaProfilo: function(idCliente: number, NOME: string, COGNOME: string, DTNASCITA: string, SESSO: string, MAIL: string,
					INDIR: string, CITTA: string, PROV: string, CAP: string, UNSUB: number, MAIL_2: string, INDIRIZZO_SPED: string,
					CAP_SPED: string, CITTA_SPED: string, PROV_SPED: string){  // /mobile/user.json?action=datiProfilo&idc=1000002
				var urlServizi;
				var modo = parseInt($rootScope.modoGioco);
				switch(modo){
					case 3:
						urlServizi = $rootScope.torneoClassicoUrl;
						break;
					case 1:
						urlServizi = $rootScope.torneoGironiUrl;
						break;
					case 2:
						urlServizi = $rootScope.torneoLegheUrl;
						break;
					case 4:
						urlServizi = $rootScope.torneoPlayoffUrl;
						break;
				}
				var deferred = $q.defer();
				$http.get(urlServizi+'mobile/user.json', {
					params: {
						action: 'updateUser',
						idc: idCliente,
						NOME: NOME,
						COGNOME: COGNOME,
						DTNASCITA: DTNASCITA,
						SESSO: SESSO,
						MAIL: MAIL,
						INDIR: INDIR,
						CITTA: CITTA,
						PROV: PROV,
						CAP: CAP,
						UNSUB: UNSUB,
						MAIL_2: MAIL_2,
						INDIRIZZO_SPED: INDIRIZZO_SPED,
						CAP_SPED: CAP_SPED,
						CITTA_SPED: CITTA_SPED,
						PROV_SPED: PROV_SPED
					}
				}).success(function(data: OM.GetProfiloResponse){
					deferred.resolve(data);
				}).error(function(data: OM.ErrorResponse){
					deferred.reject(data)
				});
				return deferred.promise;
			}
		}
	}]);

	app.service('RosaService', function($rootScope) {
		var offerte: boolean = false;
		var primaRosa: boolean = false;
		var bloccoRosa: boolean = false;
		var portieriRosa: OM.ROSA[] = [];
		var difensoriRosa: OM.ROSA[] = [];
		var centrocampistiRosa: OM.ROSA[] = [];
		var attaccantiRosa: OM.ROSA[] = [];
		var allenatoriRosa: OM.ROSA[] = [];
		var giocatoriDisponibili: OM.GIOCATORIDISPONIBILI[] = [];
		var giocatoreSelezionato: OM.GIOCATORIDISPONIBILI = null;
		var giocatoriVenduti: string = '';
		var giocatoriAcquistati: string = '';
		var saldo = 0;
		var lastSelected: {ruolo: string, index: number} = null;

		return {
			setBloccoRosa: function(value: boolean){
				bloccoRosa = value;
			},
			getBloccoRosa: function(){
				return bloccoRosa;
			},
			setOfferte: function(value: boolean){
				offerte = value;
			},
			getOfferte: function(){
				return offerte;
			},
			setPrimaRosa: function(value: boolean){
				primaRosa = value;
			},
			getPrimaRosa: function(){
				return primaRosa;
			},
			setGiocatore: function(giocatore: OM.GIOCATORIDISPONIBILI, offerta: number){
				switch(lastSelected.ruolo){
					case 'P':
						var giocatoreRosaCast: any = {};
						angular.extend(giocatoreRosaCast, giocatore);
						giocatoreRosaCast.COSTO_ACQUISTO = offerta;
						giocatoreRosaCast.IN_FORMAZIONE = false;
						portieriRosa[lastSelected.index] = giocatoreRosaCast;
						break;
					case 'D':
						var giocatoreRosaCast: any = {};
						angular.extend(giocatoreRosaCast, giocatore);
						giocatoreRosaCast.COSTO_ACQUISTO = offerta;
						giocatoreRosaCast.IN_FORMAZIONE = false;
						difensoriRosa[lastSelected.index] = giocatoreRosaCast;
						break;
					case 'C':
						var giocatoreRosaCast: any = {};
						angular.extend(giocatoreRosaCast, giocatore);
						giocatoreRosaCast.COSTO_ACQUISTO = offerta;
						giocatoreRosaCast.IN_FORMAZIONE = false;
						centrocampistiRosa[lastSelected.index] = giocatoreRosaCast;
						break;
					case 'A':
						var giocatoreRosaCast: any = {};
						angular.extend(giocatoreRosaCast, giocatore);
						giocatoreRosaCast.COSTO_ACQUISTO = offerta;
						giocatoreRosaCast.IN_FORMAZIONE = false;
						attaccantiRosa[lastSelected.index] = giocatoreRosaCast;
						break;
					case 'AL':
						var giocatoreRosaCast: any = {};
						angular.extend(giocatoreRosaCast, giocatore);
						giocatoreRosaCast.COSTO_ACQUISTO = offerta;
						giocatoreRosaCast.IN_FORMAZIONE = false;
						allenatoriRosa[lastSelected.index] = giocatoreRosaCast;
						break;
				}
				giocatoriDisponibili.splice(giocatoriDisponibili.indexOf(giocatore), 1);
			},
			giocatoreRemovedFromRosa: function(giocatore: OM.ROSA){
				var rosaGiocatoreCast: any = {};
				angular.extend(rosaGiocatoreCast, giocatore);
				//rosaGiocatoreCast.VALORE_ATTUALE = giocatore.COSTO_ACQUISTO;
				delete rosaGiocatoreCast.COSTO_ACQUISTO;
				delete rosaGiocatoreCast.IN_FORMAZIONE;
				giocatoriDisponibili.push(rosaGiocatoreCast);
			},
			getPortieri: function(sortBy: string, crescente: boolean, squadra: string[], costoMin: number, costoMax: number){
				var giocatori = [];
				if(giocatoriDisponibili.length > 0){
					giocatoriDisponibili.forEach(function(item){
						if(item.RUOLO == 0 && (squadra.length == 0 || squadra.indexOf(item.SQUADRA) > -1 ) && 
							(item.VALORE_ATTUALE >= costoMin && item.VALORE_ATTUALE <= costoMax)){
							giocatori.push(item);
						}
					});
				}

				if(sortBy.toLowerCase() == "cognome"){
					if(crescente){
						giocatori.sort(compareByCognomeCrescente);
					} else {
						giocatori.sort(compareByCognomeDecrescente)
					}
				} else if(sortBy.toLowerCase() == "costo"){
					if(crescente){
						giocatori.sort(compareByCostoCrescente);
					} else {
						giocatori.sort(compareByCostoDecrescente);
					}
				} else {
					return [];
				}
				return giocatori;
			},
			getDifensori: function(sortBy: string, crescente: boolean, squadra: string[], costoMin: number, costoMax: number){
				var giocatori = [];
				if(giocatoriDisponibili.length > 0){
					giocatoriDisponibili.forEach(function(item){
						if(item.RUOLO == 1 && (squadra.length == 0 || squadra.indexOf(item.SQUADRA) > -1) && 
							(item.VALORE_ATTUALE >= costoMin && item.VALORE_ATTUALE <= costoMax)){
							giocatori.push(item);
						}
					});
				}
				if(sortBy.toLowerCase() == "cognome"){
					if(crescente){
						giocatori.sort(compareByCognomeCrescente);
					} else {
						giocatori.sort(compareByCognomeDecrescente)
					}
				} else if(sortBy.toLowerCase() == "costo"){
					if(crescente){
						giocatori.sort(compareByCostoCrescente);
					} else {
						giocatori.sort(compareByCostoDecrescente);
					}
				} else {
					return [];
				}
				return giocatori;
			},
			getCentrocampisti: function(sortBy: string, crescente: boolean, squadra: string[], costoMin: number, costoMax: number){
				var giocatori = [];
				if(giocatoriDisponibili.length > 0){
					giocatoriDisponibili.forEach(function(item){
						if(item.RUOLO == 2 && (squadra.length == 0 || squadra.indexOf(item.SQUADRA) > -1) && 
							(item.VALORE_ATTUALE >= costoMin && item.VALORE_ATTUALE <= costoMax)){
							giocatori.push(item);
						}
					});
				}
				if(sortBy.toLowerCase() == "cognome"){
					if(crescente){
						giocatori.sort(compareByCognomeCrescente);
					} else {
						giocatori.sort(compareByCognomeDecrescente)
					}
				} else if(sortBy.toLowerCase() == "costo"){
					if(crescente){
						giocatori.sort(compareByCostoCrescente);
					} else {
						giocatori.sort(compareByCostoDecrescente);
					}
				} else {
					return [];
				}
				return giocatori;
			},
			getAttaccanti: function(sortBy: string, crescente: boolean, squadra: string[], costoMin: number, costoMax: number){
				var giocatori = [];
				if(giocatoriDisponibili.length > 0){
					giocatoriDisponibili.forEach(function(item){
						if(item.RUOLO == 3 && (squadra.length == 0 || squadra.indexOf(item.SQUADRA) > -1) && 
							(item.VALORE_ATTUALE >= costoMin && item.VALORE_ATTUALE <= costoMax)){
							giocatori.push(item);
						}
					});
				}
				if(sortBy.toLowerCase() == "cognome"){
					if(crescente){
						giocatori.sort(compareByCognomeCrescente);
					} else {
						giocatori.sort(compareByCognomeDecrescente)
					}
				} else if(sortBy.toLowerCase() == "costo"){
					if(crescente){
						giocatori.sort(compareByCostoCrescente);
					} else {
						giocatori.sort(compareByCostoDecrescente);
					}
				} else {
					return [];
				}
				return giocatori;
			},
			getAllenatori: function(sortBy: string, crescente: boolean, squadra: string[], costoMin: number, costoMax: number){
				var giocatori = [];
				if(giocatoriDisponibili.length > 0){
					giocatoriDisponibili.forEach(function(item){
						if(item.RUOLO == 4 && (squadra.length == 0 || squadra.indexOf(item.SQUADRA) > -1) && 
							(item.VALORE_ATTUALE >= costoMin && item.VALORE_ATTUALE <= costoMax)){
							giocatori.push(item);
						}
					});
				}
				if(sortBy.toLowerCase() == "cognome"){
					if(crescente){
						giocatori.sort(compareByCognomeCrescente);
					} else {
						giocatori.sort(compareByCognomeDecrescente)
					}
				} else if(sortBy.toLowerCase() == "costo"){
					if(crescente){
						giocatori.sort(compareByCostoCrescente);
					} else {
						giocatori.sort(compareByCostoDecrescente);
					}
				} else {
					return [];
				}
				return giocatori;
			},
			setRosa: function(portieri: OM.ROSA[], difensori: OM.ROSA[], centrocampisti: OM.ROSA[], attaccanti: OM.ROSA[], allenatori?: OM.ROSA[]){
				portieriRosa = portieri;
				difensoriRosa = difensori;
				centrocampistiRosa = centrocampisti;
				attaccantiRosa = attaccanti;
				if($rootScope.misterCalcioCup)
					allenatoriRosa = allenatori;
			},
			getRosa: function(){
				if(!$rootScope.mistercalciocup)
					return portieriRosa.concat(difensoriRosa, centrocampistiRosa, attaccantiRosa, allenatoriRosa);
				else($rootScope.mistercalciocup)
					return portieriRosa.concat(difensoriRosa, centrocampistiRosa, attaccantiRosa, allenatoriRosa, allenatoriRosa);
			},
			getSquadre: function(){
				var elencoSquadre: string[] = [];
				giocatoriDisponibili.forEach(function(item){
					if(elencoSquadre.indexOf(item.SQUADRA) == -1){
						elencoSquadre.push(item.SQUADRA);
					}
				});
				return elencoSquadre;
			},
			setGiocatoriDisponibili: function(_giocatoriDisponibili: OM.GIOCATORIDISPONIBILI[]){
				giocatoriDisponibili = _giocatoriDisponibili;
			},            
			getGiocatoriDisponibili: function(){
				return giocatoriDisponibili;
			},
			setGiocatoreSelezionato: function(_giocatoreSelezionato: OM.GIOCATORIDISPONIBILI){
				giocatoreSelezionato = _giocatoreSelezionato;
			},
			getGiocatoreSelezionato: function(){
				return giocatoreSelezionato;
			},
			setGiocatoriVenduti: function(_giocatoriVenduti: string){
				giocatoriVenduti = _giocatoriVenduti;
			},
			getGiocatoriVenduti: function(){
				return giocatoriVenduti;
			},
			setGiocatoriAcquistati: function(_giocatoriAcquistati: string){
				giocatoriAcquistati = _giocatoriAcquistati;
			},
			getGiocatoriAcquistati: function(){
				return giocatoriAcquistati;
			},
			setLastSelected: function(_lastSelected: {ruolo: string; index: number}){
				lastSelected = _lastSelected;
			},
			getLastSelected: function(){
				return lastSelected;
			},
			setSaldo: function(_saldo:number){
				saldo = _saldo;
			},
			getSaldo: function(){
				return saldo;
			}
		};

		//FUNZIONI PER IL SORTING
		function compareByCognomeCrescente(a: OM.GIOCATORIDISPONIBILI, b: OM.GIOCATORIDISPONIBILI) {
			if (a.COGNOME < b.COGNOME)
				return -1;
			if (a.COGNOME > b.COGNOME)
				return 1;
			return 0;
		};
		function compareByCostoCrescente(a: OM.GIOCATORIDISPONIBILI, b: OM.GIOCATORIDISPONIBILI) {
			if (a.VALORE_ATTUALE < b.VALORE_ATTUALE)
				return -1;
			if (a.VALORE_ATTUALE > b.VALORE_ATTUALE)
				return 1;
			return 0;
		};
		function compareByCognomeDecrescente(a: OM.GIOCATORIDISPONIBILI, b: OM.GIOCATORIDISPONIBILI) {
			if (a.COGNOME > b.COGNOME)
				return -1;
			if (a.COGNOME < b.COGNOME)
				return 1;
			return 0;
		};
		function compareByCostoDecrescente(a: OM.GIOCATORIDISPONIBILI, b: OM.GIOCATORIDISPONIBILI) {
			if (a.VALORE_ATTUALE > b.VALORE_ATTUALE)
				return -1;
			if (a.VALORE_ATTUALE < b.VALORE_ATTUALE)
				return 1;
			return 0;
		};
	});


})();