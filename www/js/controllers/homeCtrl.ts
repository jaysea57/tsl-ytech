/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function(){
	var app = angular.module('homeCtrl', []);

	app.controller('homeCtrl', function($scope, $rootScope, $state, $ionicLoading, $ionicHistory, $ionicModal, $cordovaGoogleAnalytics,
	FantaCalcioServices: OM.FantaCalcioFactory) {

	$rootScope.rosaInCorso = false;
	$scope.$on('$ionicView.enter', function(){
		console.log("$on('$ionicView.enter')");
		if (window.cordova) $cordovaGoogleAnalytics.trackView('Dashboard');
		onViewEnter();
	});

	function onViewEnter(){


		$ionicHistory.clearHistory;
		FantaCalcioServices.getSquadre($rootScope.user.idCliente, $rootScope.modoGioco).then(function(data){
			if(data.error){
				$rootScope.showToast(data.error.user_message);
				//$ionicLoading.show({
				//	template: data.error.user_message,
				//	noBackdrop: true,
				//	duration: 2300
				//});
                if(data.error && data.error.id == 'not_logged')
                    $rootScope.autologin();
			}else{
				console.log(data);
				//$ionicLoading.show({
				//	template: $rootScope.squadraSelected.nome,
				//	noBackdrop: true,
				//	duration: 2300
				//});
				if ($rootScope.modoGioco == 3) {
					$rootScope.squadreTorneoClassico = data.squadre;
					if ($rootScope.misterCalcioCup) {
						window.localStorage.setItem('misterCalcioCupSquadreTorneoClassico' + $rootScope.user.idCliente, JSON.stringify(data.squadre));
					} else {
						window.localStorage.setItem('tuttoSportLeagueSquadreTorneoClassico' + $rootScope.user.idCliente, JSON.stringify(data.squadre));
					}
				} else {
					$rootScope.squadreTorneoGironi = data.squadre;
					if ($rootScope.misterCalcioCup) {
						window.localStorage.setItem('misterCalcioCupSquadreTorneoGironi' + $rootScope.user.idCliente, JSON.stringify(data.squadre));
					} else {
						window.localStorage.setItem('tuttoSportLeagueSquadreTorneoGironi' + $rootScope.user.idCliente, JSON.stringify(data.squadre));
					}
				}

				//console.log("squadraSelected: " + $rootScope.squadraSelected);
				if (!$rootScope.squadraSelected || $rootScope.secondaLogin == true) {
					console.log("not sq selected");
					$rootScope.secondaLogin = false;
					$rootScope.squadraSelected = data.squadre[0];
					//if($rootScope.misterCalcioCup)
					//	window.localStorage.setItem('misterCalcioCupModoGioco', $rootScope.modoGioco);
					//else
					//	window.localStorage.setItem('tuttoSportLeagueModoGioco', $rootScope.modoGioco);
				}
				else { // jc 2017
					for (let sq of data.squadre) {
						if (sq.codice_squadra == $rootScope.squadraSelected.codice_squadra) {
							console.log("faccio refresh of squadra selected");
							$rootScope.squadraSelected = sq;
						}
					}
				}
				if ($rootScope.misterCalcioCup) {
					window.localStorage.setItem('misterCalcioCupSquadraSelected' + $rootScope.user.idCliente, JSON.stringify($rootScope.squadraSelected));
				} else {
					window.localStorage.setItem('tuttoSportLeagueSquadraSelected' + $rootScope.user.idCliente, JSON.stringify($rootScope.squadraSelected));
				}
				} // else data error
				if($rootScope.squadraSelected.playoff){
					$rootScope.modoGioco = 4;
				}
				if(!$rootScope.iscriviPlayoffShown){
					if($rootScope.squadraSelected.iscrizioni_playoff_aperte && $rootScope.squadraSelected.qualificataPlayoff && $rootScope.squadraSelected.iscrittaPlayoff == false){
						navigator.notification.confirm($rootScope.squadraSelected.msg_iscrizione_playoff, function(buttonPressed: number){
							if(buttonPressed == 1){
								FantaCalcioServices.registraAPlayoff($rootScope.squadraSelected.codice_squadra).then(function(data){
									$rootScope.showLoading();
									FantaCalcioServices.getSquadre($rootScope.user.idCliente, $rootScope.modoGioco).then(function(dataSquadre){
										dataSquadre.squadre.forEach(function(item){
											if(data.success.id_squadra == item.codice_squadra)
												$rootScope.squadraSelected = item;
										});
										$rootScope.hideLoading();
										navigator.notification.alert(data.success.msg_utente, function () { }, 'Iscrizione ai Playoff');
									},function(error){
										$rootScope.hideLoading();
										console.log("error getSquadre");
										console.log(error);
									});
								}, function(errorData){
						        	$rootScope.hideLoading();
									$rootScope.showToast(errorData.user_message);
								});
							}
							$rootScope.iscriviPlayoffShown = true;
						}, 'Iscrizione ai Playoff', ['Iscrivila', 'Non ora']);
					}
				}

			if($rootScope.hideNews.value == false){
				$ionicModal.fromTemplateUrl('newsModal.html', {
					scope: $scope,
					animation: 'slide-in-up'
				}).then(function(modal) {
					$scope.modal = modal;
					$scope.news = [];
					FantaCalcioServices.getNews().then(function(data: OM.GetNewsResponse){
						if(data.listaNews && data.listaNews.length > 0){
							var newsRead: OM.News[] = [];
							if($rootScope.misterCalcioCup){
								newsRead = JSON.parse(window.localStorage.getItem('misterCalcioCupNewsRead'+$rootScope.user.idCliente));
							} else {
								newsRead = JSON.parse(window.localStorage.getItem('tuttoSportLeagueNewsRead'+$rootScope.user.idCliente));
							}
							$rootScope.hideLoading();
							if(newsRead){
								newsRead.forEach(function(itemRead){
									data.listaNews.forEach(function(itemLista, iLista){
										if(itemLista.idNews == itemRead.idNews){
											data.listaNews.splice(iLista,1);
										}
									});
								});
							}
							$scope.news = data.listaNews;
							if($scope.news.length){
								$scope.modal.show();
							}
						}
					});
					$scope.closeNewsModal = function(){
						if($rootScope.hideNews.value == true){
							if($rootScope.misterCalcioCup){
								window.localStorage.setItem('misterCalcioCupNewsRead'+$rootScope.user.idCliente, JSON.stringify($scope.news));
							} else {
								window.localStorage.setItem('tuttoSportLeagueNewsRead'+$rootScope.user.idCliente, JSON.stringify($scope.news));
							} 
						}
						$scope.modal.hide(); 
					};
				});
			}
    		$rootScope.hideLoading();    		
	  	},function(data){
        	$rootScope.hideLoading();
			$rootScope.showToast('Errore nella connessione');
		})

	};

	$scope.goToFormazione = function(){
		$rootScope.showLoading();
		$state.go('app.formazione');
	};
	$scope.goToDettaglioGiornata = function(){
		$rootScope.showLoading();
		if($rootScope.modoGioco == 3){
			console.log($rootScope.squadraSelected);
			$rootScope.giornataClassico = {id_partita: $rootScope.squadraSelected.ultima_partita.id};
	  		$state.go('app.dettaglioGiornata', {
	  			idGiornata: $rootScope.squadraSelected.ultima_partita.id, 
	  			dataGiornata: $rootScope.squadraSelected.ultima_partita.data,
	  			numeroGiornata: $rootScope.squadraSelected.ultima_partita.numero
	  			});
		} else {
			$rootScope.partitaDettaglioGiornata = $rootScope.squadraSelected.ultima_partita;
	  		$state.go('app.dettaglioGiornataGironi', {idGiornata: $rootScope.squadraSelected.ultima_partita.id, 
	  			dataGiornata: $rootScope.squadraSelected.ultima_partita.data, numeroGiornata: $rootScope.squadraSelected.ultima_partita.numero});
	  	}
	};
	$scope.goToGestioneRosa = function(){
		$rootScope.showLoading();
	  	$state.go('app.gestioneRosa');
	};
	$scope.goToClassificaGironi = function(){
		$rootScope.showLoading();
	  	$state.go('app.classificheGironi');
	};

  });

  })();