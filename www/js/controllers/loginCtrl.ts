/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />

(function () {
	var app = angular.module('loginCtrl', []);
	app.controller('loginCtrl', function ($scope, $rootScope, $state, $ionicLoading, $stateParams, $ionicHistory, $cordovaGoogleAnalytics,
		FantaCalcioServices: OM.FantaCalcioFactory) {
		console.log(">loginCtrl");
		$scope.vm = {
			email: '',
			password: '',
			modoGioco: $stateParams.gioco
		};

		//if($rootScope.misterCalcioCup)
		//	$rootScope.modoGioco = window.localStorage.getItem('misterCalcioCupModoGioco');
		//else
		//	$rootScope.modoGioco = window.localStorage.getItem('tuttoSportLeagueModoGioco');

		//Inserito per prevenire l'errore "Sistema sovraccarico, impossibile effettuare l'accesso"
		$scope.counterLogin = 0;

		if (window.cordova) {
			if($stateParams.gioco == 3){
				$cordovaGoogleAnalytics.trackView('Login Classico');
			} else {
				$cordovaGoogleAnalytics.trackView('Login Gironi');
			}
		}

		$scope.login = function(){
			if($scope.counterLogin == 0)
				$rootScope.showLoading("Login in corso");
			$scope.counterLogin ++;
			FantaCalcioServices.authentication($scope.vm.email, $scope.vm.password, $scope.vm.modoGioco, $rootScope.deviceToken, $rootScope.deviceId, $rootScope.deviceTarget).then(function(data:OM.AuthenticationResponse){
				if(data.success){
					console.log("authentication ok.. response ws segue");
					console.log(data);
					$rootScope.modoGioco = $scope.vm.modoGioco;
					if($rootScope.misterCalcioCup){
						if($scope.vm.modoGioco == 3){
							window.localStorage.setItem('misterCalcioCupUserLoginClassico', JSON.stringify($scope.vm));
							window.localStorage.setItem('misterCalcioCupUserTorneoClassico', JSON.stringify(data.success.user));
							$rootScope.userClassico = data.success.user;
						} else {
							window.localStorage.setItem('misterCalcioCupUserLoginGironi', JSON.stringify($scope.vm));
							window.localStorage.setItem('misterCalcioCupUserTorneoGironi', JSON.stringify(data.success.user));
							$rootScope.userGironi = data.success.user;
						}
						window.localStorage.setItem('misterCalcioCupUserLogin', JSON.stringify($scope.vm));
					} else {
						if($scope.vm.modoGioco == 3){
							window.localStorage.setItem('tuttoSportLeagueUserLoginClassico', JSON.stringify($scope.vm));
							window.localStorage.setItem('tuttoSportLeagueUserTorneoClassico', JSON.stringify(data.success.user));
							$rootScope.userClassico = data.success.user;
						} else {
							window.localStorage.setItem('tuttoSportLeagueUserLoginGironi', JSON.stringify($scope.vm));
							window.localStorage.setItem('tuttoSportLeagueUserTorneoGironi', JSON.stringify(data.success.user));
							$rootScope.userGironi = data.success.user;
						}
						window.localStorage.setItem('tuttoSportLeagueUserLogin', JSON.stringify($scope.vm));
					}
					$rootScope.user = data.success.user;
					$ionicHistory.nextViewOptions({
                        disableAnimate: true,
                        disableBack: true,
                        historyRoot: true
                    });
					if (window.cordova) {
						AdMob.showInterstitial(function () {
							AdMob.prepareInterstitial({
								adId: $rootScope.admobid.interstitial,
								autoShow: false
							});
							$rootScope.showBanner();
							$state.go('app.home');
						}, function () {
							AdMob.prepareInterstitial({
								adId: $rootScope.admobid.interstitial,
								autoShow: false
							});
							$rootScope.showBanner();
							$state.go('app.home');
						});
						
					}
					else {
						$state.go('app.home');
					}
				
				}else if(data.error){
					console.log("data.error");
					console.log(JSON.stringify(data));
					if ($scope.counterLogin <= 3) {
						$scope.login();
					} else {
						$rootScope.hideLoading();
						$rootScope.showToast(data.error.user_message, 5000);
					}
				}
			},function(data){
				$rootScope.hideLoading();
				$rootScope.showToast('Errore nella connessione');
				console.log("Errore in connessione");
				console.log(JSON.stringify(data));
			});
		};

		$scope.goBack = function(){
			window.history.back();
		};
		$scope.facebookLogin = function () {
			if (!ionic.Platform.isIOS()) {
				$rootScope.showLoading();
			}
			/* jc lug 2017 usiamo plugin standard mono app id */
			/*
			var appId;
			if($scope.vm.modoGioco == 3){
				appId = '136827786413455';
			} else {
				appId = '343417689117586';
			}
			*/
			// jc tolto setApplicationId
			facebookConnectPlugin.login(['email'], function (FBSuccessResponse) {
				console.log("facebook login success data: ");
				console.log(JSON.stringify(FBSuccessResponse));
				FantaCalcioServices.facebookAuthentication($scope.vm.email, '',
					$scope.vm.modoGioco, FBSuccessResponse.authResponse.accessToken
					, $rootScope.deviceToken, $rootScope.deviceId, $rootScope.deviceTarget)
					.then(function (data: OM.AuthenticationResponse) {
						if (data.success) {
							console.log(data);
							$rootScope.modoGioco = $scope.vm.modoGioco;
							if ($rootScope.misterCalcioCup) {
								window.localStorage.setItem('misterCalcioCupUserLogin', JSON.stringify($scope.vm));
								if ($scope.vm.modoGioco == 3) {
									window.localStorage.setItem('misterCalcioCupUserTorneoClassico', JSON.stringify(data.success.user));
									$rootScope.userClassico = data.success.user;
								} else {
									window.localStorage.setItem('misterCalcioCupUserTorneoGironi', JSON.stringify(data.success.user));
									$rootScope.userGironi = data.success.user;
								}
							} else {
								window.localStorage.setItem('tuttoSportLeagueUserLogin', JSON.stringify($scope.vm));
								if ($scope.vm.modoGioco == 3) {
									window.localStorage.setItem('tuttoSportLeagueUserTorneoClassico', JSON.stringify(data.success.user));
									$rootScope.userClassico = data.success.user;
								} else {
									window.localStorage.setItem('tuttoSportLeagueUserTorneoGironi', JSON.stringify(data.success.user));
									$rootScope.userGironi = data.success.user;
								}
							}
							$rootScope.user = data.success.user;
							$rootScope.accessToken = data.success.user.accessToken;
							if (data.success.required_reg == 1) {
								if (data.success.required_email == 1) {
									if (window.cordova) {
										AdMob.showInterstitial(function () {
											AdMob.prepareInterstitial({
												adId: $rootScope.admobid.interstitial,
												autoShow: false
											});
											$rootScope.showBanner();
											$state.go('requiredReg', { requiredMail: 1 });
										}, function () {
											AdMob.prepareInterstitial({
												adId: $rootScope.admobid.interstitial,
												autoShow: false
											});
											$rootScope.showBanner();
											$state.go('requiredReg', { requiredMail: 1 });
										});
									}
									else {
										$state.go('requiredReg', { requiredMail: 1 });
									}
								}
								else {
									if (window.cordova) {
										AdMob.showInterstitial(function () {
											AdMob.prepareInterstitial({
												adId: $rootScope.admobid.interstitial,
												autoShow: false
											});
											$rootScope.showBanner();
											$state.go('requiredReg', { requiredMail: 0 });
										}, function () {
											AdMob.prepareInterstitial({
												adId: $rootScope.admobid.interstitial,
												autoShow: false
											});
											$rootScope.showBanner();
											$state.go('requiredReg', { requiredMail: 0 });
										});

									}
									else {
										$state.go('requiredReg', { requiredMail: 0 });
									}
								}
							} else {
								$ionicHistory.nextViewOptions({
									disableAnimate: true,
									disableBack: true,
									historyRoot: true
								});
								if (window.cordova) {
									AdMob.showInterstitial(function () {
										AdMob.prepareInterstitial({
											adId: $rootScope.admobid.interstitial,
											autoShow: false
										});
										$rootScope.showBanner();
										$state.go('app.home');
									}, function () {
										AdMob.prepareInterstitial({
											adId: $rootScope.admobid.interstitial,
											autoShow: false
										});
										$rootScope.showBanner();
										$state.go('app.home');
									});
								}
								else {
									$state.go('app.home');
								}
							}
						} else if (data.error) {
							console.log("data error");
							console.log(JSON.stringify(data.error));
							$rootScope.hideLoading();
							$rootScope.showToast(data.error.user_message);
						}
						$rootScope.hideLoading();
					}, function (data) {
						$rootScope.hideLoading();
						$rootScope.showToast('Errore nella connessione');
						console.log("errore in chiamata FantaCalcioServices.facebookAuthentication server side service error data: ");
						console.log(JSON.stringify(data));
					});
			}, function (FBErrorResponse) {
				$rootScope.hideLoading();
				$rootScope.showToast("Errore nella connessione con facebook");
				console.log("facebook login error data: ");
				console.log(JSON.stringify(FBErrorResponse));
			});

		};

		$scope.goRegistrazione = function(){
			$rootScope.showLoading();
			$state.go('registrazione', {gioco: $stateParams.gioco});
		};
		if($rootScope.hideLoading)
			$rootScope.hideLoading();

	});
})();