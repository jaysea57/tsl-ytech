/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
	var app = angular.module('formazioneCtrl', []);
	app.controller('formazioneCtrl', function ($scope, $rootScope, $ionicPopover, $ionicLoading, $cordovaGoogleAnalytics,
		FantaCalcioServices: OM.FantaCalcioFactory) {

		if (window.cordova) $cordovaGoogleAnalytics.trackView('Gestione Formazione');

		$scope.vm = {
			portieri: [],
			portieriPosition: {value: null},
			portieriPanchina: [],
			portieriPanchinaPosition: {value: null},
			difensori: [],
			difensoriPosition: {value: null},
			difensoriPanchina: [],
			difensoriPanchinaPosition: {value: null},
			centrocampisti: [],
			centrocampistiPosition: {value: null},
			centrocampistiPanchina: [],
			centrocampistiPanchinaPosition: {value: null},
			attaccanti: [],
			attaccantiPosition: {value: null},
			attaccantiPanchina: [],
			attaccantiPanchinaPosition: {value: null},
			allenatoriPosition: {value: null},
			allenatoriPanchina: [],
			allenatoriPanchinaPosition: {value: null},
			portieriRosa: [],
			difensoriRosa: [],
			centrocampistiRosa: [],
			attaccantiRosa: [],
			allenatoriRosa: [],
			tattiche: [],
			tatticaSelected: {
				DESCRIZIONE: "4-4-2",
				DESIGN: {
					ATTACCANTI: 2,
					CENTROCAMPISTI: 4,
					DIFENSORI: 4,
				},
				TATTICA: 5
			},
			rosaShown: {ruolo:'', position: null},
			rosaPanchinaShown: '',
			ruoloGiocatoreSelected: '',
			formazione: '',
			tipoBlocco: 'nessuno'
		};

		if($rootScope.misterCalcioCup){
			$scope.vm.allenatori = [];
			$scope.vm.allenatoriPosition = {value: null};
			$scope.vm.allenatoriPanchina = [];
			$scope.vm.allenatoriPanchinaPosition = {value: null};
			$scope.vm.allenatoriRosa = [];
		}

		function readFormazione(formazioneData: number[]){
			if(formazioneData){
				if($rootScope.misterCalcioCup){
					$scope.vm.allenatori = [];
					formazioneData.forEach(function(item, index){
						var itemAppoggio: OM.RosaFormazioneData = null;
						if(index == formazioneData.length-1){
							$scope.vm.formazione += item + ':' + index;
						} else {
							$scope.vm.formazione += item + ':' + index + ';';
						}
						if(index == 0){
							itemAppoggio = getGiocatoreById($scope.vm.portieriRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.portieri[0] = itemAppoggio;
							//aggiungo +1 che è il portiere
						} else if(index < 1 + $scope.vm.tatticaSelected.DESIGN.DIFENSORI) {
							itemAppoggio = getGiocatoreById($scope.vm.difensoriRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.difensori.push(itemAppoggio);
						} else if(index < 1 + $scope.vm.tatticaSelected.DESIGN.DIFENSORI + $scope.vm.tatticaSelected.DESIGN.CENTROCAMPISTI) {
							itemAppoggio = getGiocatoreById($scope.vm.centrocampistiRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.centrocampisti.push(itemAppoggio);
						} else if(index < 1 + $scope.vm.tatticaSelected.DESIGN.DIFENSORI + $scope.vm.tatticaSelected.DESIGN.CENTROCAMPISTI + $scope.vm.tatticaSelected.DESIGN.ATTACCANTI) {
							itemAppoggio = getGiocatoreById($scope.vm.attaccantiRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.attaccanti.push(itemAppoggio);
						} else if(index <= 11) {
							itemAppoggio = getGiocatoreById($scope.vm.portieriRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.portieriPanchina.push(itemAppoggio);
						} else if(index <= 13) {
							itemAppoggio = getGiocatoreById($scope.vm.difensoriRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.difensoriPanchina.push(itemAppoggio);
						} else if(index <= 15) {
							itemAppoggio = getGiocatoreById($scope.vm.centrocampistiRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.centrocampistiPanchina.push(itemAppoggio);
						} else if(index <= 17) {
							itemAppoggio = getGiocatoreById($scope.vm.attaccantiRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.attaccantiPanchina.push(itemAppoggio);
						} else if(index <= 18) {
							itemAppoggio = getGiocatoreById($scope.vm.allenatoriRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.allenatori.push(itemAppoggio);
						} else if(index <= 19) {
							itemAppoggio = getGiocatoreById($scope.vm.allenatoriRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.allenatoriPanchina.push(itemAppoggio);
						} 
					});
				} else {
					formazioneData.forEach(function(item, index){
						var itemAppoggio: OM.RosaFormazioneData = null;
						if(index == formazioneData.length-1){
							$scope.vm.formazione += item + ':' + index;
						} else {
							$scope.vm.formazione += item + ':' + index + ';';
						}
						if(index == 0){
							itemAppoggio = getGiocatoreById($scope.vm.portieriRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.portieri[0] = itemAppoggio;
							//aggiungo +1 che è il portiere
						} else if(index < 1 + $scope.vm.tatticaSelected.DESIGN.DIFENSORI) {
							itemAppoggio = getGiocatoreById($scope.vm.difensoriRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.difensori.push(itemAppoggio);
						} else if(index < 1 + $scope.vm.tatticaSelected.DESIGN.DIFENSORI + $scope.vm.tatticaSelected.DESIGN.CENTROCAMPISTI) {
							itemAppoggio = getGiocatoreById($scope.vm.centrocampistiRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.centrocampisti.push(itemAppoggio);
						} else if(index < 1 + $scope.vm.tatticaSelected.DESIGN.DIFENSORI + $scope.vm.tatticaSelected.DESIGN.CENTROCAMPISTI + $scope.vm.tatticaSelected.DESIGN.ATTACCANTI) {
							itemAppoggio = getGiocatoreById($scope.vm.attaccantiRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.attaccanti.push(itemAppoggio);
						} else if(index <= 11) {
							itemAppoggio = getGiocatoreById($scope.vm.portieriRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.portieriPanchina.push(itemAppoggio);
						} else if(index <= 13) {
							itemAppoggio = getGiocatoreById($scope.vm.difensoriRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.difensoriPanchina.push(itemAppoggio);
						} else if(index <= 15) {
							itemAppoggio = getGiocatoreById($scope.vm.centrocampistiRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.centrocampistiPanchina.push(itemAppoggio);
						} else if(index <= 17) {
							itemAppoggio = getGiocatoreById($scope.vm.attaccantiRosa, item);
							itemAppoggio.inFormazione = true;
							$scope.vm.attaccantiPanchina.push(itemAppoggio);
						} 
					});
				}
			} else if (formazioneData == undefined){
				$scope.vm.portieriPanchina.length = 1;
				$scope.vm.difensoriPanchina.length = 2;
				$scope.vm.centrocampistiPanchina.length = 2;
				$scope.vm.attaccantiPanchina.length = 2;
				if($rootScope.misterCalcioCup){
					$scope.vm.allenatoriPanchina.length = 1;
				}
			}
			
		};

		$scope.init = function(){
			console.log("user!");
			console.log($rootScope.user);
			FantaCalcioServices.formazioneData($rootScope.modoGioco, $rootScope.squadraSelected.codice_squadra).then(function(data){
				if(data.error){
					console.log(data);
                	$rootScope.showToast(data.error.user_message);
                    if(data.error && data.error.id == 'not_logged')
                        $rootScope.autologin();
				} else {
					console.log("Formazione response!");
					console.log(data);
					$scope.vm.tattiche = data.TATTICHE;
					$scope.vm.tipoBlocco = data.tipo_blocco;
					data.ROSA.forEach(function(item){
						switch(item.CODICE_RUOLO){
							case 0:
								if(item.IN_ROSA == 1){
									item.inFormazione = false;
									$scope.vm.portieriRosa.push(item);
								}
								break;
							case 1:
								if(item.IN_ROSA == 1){
									item.inFormazione = false;
									$scope.vm.difensoriRosa.push(item);
								}
								break;
							case 2:
								if(item.IN_ROSA == 1){
									item.inFormazione = false;
									$scope.vm.centrocampistiRosa.push(item);
								}
								break;
							case 3:
								if(item.IN_ROSA == 1){
									item.inFormazione = false;
									$scope.vm.attaccantiRosa.push(item);
								}
								break;
							case 4:
								if($rootScope.misterCalcioCup){
									if(item.IN_ROSA == 1){
										item.inFormazione = false;
										$scope.vm.allenatoriRosa.push(item);
									}
								}
								break;
						};
					});
					if($scope.vm.tipoBlocco != 'totale'){
						$scope.openRosa = _openRosa;
						$scope.setPosition = _setPosition;
						$scope.openRosaPanchina = _openRosaPanchina;
						$scope.popTattiche = _popTattiche;
					}
					if(data.FORMAZIONE.length > 0){
						if(data.FORMAZIONE[0].TATTICA){
							data.TATTICHE.some(function(item){
								if(item.TATTICA == data.FORMAZIONE[0].TATTICA){
									$scope.vm.tatticaSelected = item;
									return true;
								}
							});
							} else {
							data.TATTICHE.some(function(item){
								if(item.TATTICA == data.FORMAZIONE[0][2]){
									$scope.vm.tatticaSelected = item;
									return true;
								}
							});

							}
						if(data.FORMAZIONE[0].FORMAZIONE && data.FORMAZIONE[0].FORMAZIONE.length > 0){
							$scope.vm.portieri = [];
							$scope.vm.difensori = [];
							$scope.vm.centrocampisti = [];
							$scope.vm.attaccanti = [];
							readFormazione(data.FORMAZIONE[0].FORMAZIONE)
						} else {
							//$scope.vm.portieriPanchina.length = 1;
							//$scope.vm.difensoriPanchina.length = 2;
							//$scope.vm.centrocampistiPanchina.length = 2;
							//$scope.vm.attaccantiPanchina.length = 2;
							//if($rootScope.misterCalcioCup)
							//	$scope.vm.allenatoriPanchina.length = 1;
							readFormazione(data.FORMAZIONE[0][4]);
						}
					} else {
						$scope.vm.portieriPanchina.length = 1;
						$scope.vm.difensoriPanchina.length = 2;
						$scope.vm.centrocampistiPanchina.length = 2;
						$scope.vm.attaccantiPanchina.length = 2;
						if($rootScope.misterCalcioCup)
							$scope.vm.allenatoriPanchina.length = 1;
					}
					$scope.selectTattica($scope.vm.tatticaSelected);
					$rootScope.hideLoading('');
					if(data.msg_blocco){
                		$rootScope.showToast(data.msg_blocco);
					}
				}
				$rootScope.hideLoading();
			},function(data){
				$rootScope.hideLoading();
        		$rootScope.showToast('Errore nella connessione');
			});
		};

		$scope.selectTattica = function(_tattica: OM.TatticheFormazioneData){
			$scope.vm.tatticaSelected = _tattica;
			$scope.vm.portieri.length = 1;
			$scope.vm.difensori.length = _tattica.DESIGN.DIFENSORI;
			$scope.vm.difensoriRosa.forEach(function(item, index){
				var trovato = false;
				item.inFormazione = false;
				for(var i = 0; i < $scope.vm.difensori.length && trovato == false; i++){
					if($scope.vm.difensori[i] && item.ID_GIOCATORE == $scope.vm.difensori[i].ID_GIOCATORE){
						item.inFormazione = true;
						trovato = true;
					}
				}
				for(var i = 0; i < $scope.vm.difensoriPanchina.length && trovato == false; i++){
					if($scope.vm.difensoriPanchina[i] && item.ID_GIOCATORE == $scope.vm.difensoriPanchina[i].ID_GIOCATORE){
						item.inFormazione = true;
						trovato = true;
					}
				}
			});
			$scope.vm.centrocampisti.length = _tattica.DESIGN.CENTROCAMPISTI;
			$scope.vm.centrocampistiRosa.forEach(function(item, index){
				var trovato = false;
				item.inFormazione = false;
				for(var i = 0; i < $scope.vm.centrocampisti.length && trovato == false; i++){
					if($scope.vm.centrocampisti[i] && item.ID_GIOCATORE == $scope.vm.centrocampisti[i].ID_GIOCATORE){
						item.inFormazione = true;
						trovato = true;
					}
				}
				for(var i = 0; i < $scope.vm.centrocampistiPanchina.length && trovato == false; i++){
					if($scope.vm.centrocampistiPanchina[i] && item.ID_GIOCATORE == $scope.vm.centrocampistiPanchina[i].ID_GIOCATORE){
						item.inFormazione = true;
						trovato = true;
					}
				}
			});
			$scope.vm.attaccanti.length = _tattica.DESIGN.ATTACCANTI;
			$scope.vm.attaccantiRosa.forEach(function(item, index){
				var trovato = false;
				item.inFormazione = false;
				for(var i = 0; i < $scope.vm.attaccanti.length && trovato == false; i++){
					if($scope.vm.attaccanti[i] && item.ID_GIOCATORE == $scope.vm.attaccanti[i].ID_GIOCATORE){
						item.inFormazione = true;
						trovato = true;
					}
				}
				for(var i = 0; i < $scope.vm.attaccantiPanchina.length && trovato == false; i++){
					if($scope.vm.attaccantiPanchina[i] && item.ID_GIOCATORE == $scope.vm.attaccantiPanchina[i].ID_GIOCATORE){
						item.inFormazione = true;
						trovato = true;
					}
				}
			});
			if($rootScope.misterCalcioCup){
				$scope.vm.allenatori.length = 1;
			}
		};

		function _openRosa(ruolo, indice, e){
			e.stopPropagation();
			if($scope.vm.rosaShown.ruolo == ruolo && $scope.vm.rosaShown.position == indice){
				$scope.vm.rosaShown.ruolo = '';
				$scope.vm.rosaShown.position = null;
			}else{
				$scope.vm.rosaShown.ruolo = ruolo;
				$scope.vm.rosaShown.position = indice;
			}
		};

		function _setPosition(ruoloPosition, indice, e){
			e.stopPropagation();
			$scope.vm.portieriPosition.value = null;
			$scope.vm.difensoriPosition.value = null;
			$scope.vm.centrocampistiPosition.value = null;
			$scope.vm.attaccantiPosition.value = null;
			ruoloPosition.value = indice;
		};

		$scope.setGiocatore = function(arrayGiocatori: OM.RosaFormazioneData[], giocatore: OM.RosaFormazioneData, indice, e){
			e.stopPropagation();
			if (giocatore.inFormazione == false){
				if(arrayGiocatori[indice.value] == null){
					giocatore.inFormazione = true;
					arrayGiocatori[indice.value] = giocatore;
				} else {
					giocatore.inFormazione = true;
					switch(arrayGiocatori[indice.value].CODICE_RUOLO){
						case 0:
						//cerco nella rosa il giocatore rimosso e tolgo inFormazione
							$scope.vm.portieriRosa.forEach(function(item, index){								
								if(arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE){
									$scope.vm.portieriRosa[index].inFormazione = false;
								}
							});
							break;
						case 1:
						//cerco nella rosa il giocatore rimosso e tolgo inFormazione
							$scope.vm.difensoriRosa.forEach(function(item, index){								
								if(arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE){
									$scope.vm.difensoriRosa[index].inFormazione = false;
								}
							});
							break;
						case 2:
						//cerco nella rosa il giocatore rimosso e tolgo inFormazione
							$scope.vm.centrocampistiRosa.forEach(function(item, index){								
								if(arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE){
									$scope.vm.centrocampistiRosa[index].inFormazione = false;
								}
							});
							break;
						case 3:
						//cerco nella rosa il giocatore rimosso e tolgo inFormazione
							$scope.vm.attaccantiRosa.forEach(function(item, index){								
								if(arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE){
									$scope.vm.attaccantiRosa[index].inFormazione = false;
								}
							});
							break;
						case 4:
						//cerco nella rosa il giocatore rimosso e tolgo inFormazione
							$scope.vm.allenatoriRosa.forEach(function(item, index){								
								if(arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE){
									$scope.vm.allenatoriRosa[index].inFormazione = false;
								}
							});
							break;
					};
					arrayGiocatori[indice.value] = giocatore;
				}
			} else {
				if(giocatore != arrayGiocatori[indice.value]){
					switch(giocatore.CODICE_RUOLO){
						case 0:
						//se la posizione selezionata non è vuota, tolgo inFormazione dal giocatore nella rosa
							if(arrayGiocatori[indice.value] != null){
								$scope.vm.portieriRosa.forEach(function(item, index){								
									if(arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE){
										$scope.vm.portieriRosa[index].inFormazione = false;
									}
								});
							}
						//cerco il giocatore in campo e in panchina, quando lo trovo, nullizzo
							$scope.vm.portieriPanchina.forEach(function(item,index){
								if(item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE){
									$scope.vm.portieriPanchina[index] = null;
								}
							});
							$scope.vm.portieri.forEach(function(item,index){
								if(item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE){
									$scope.vm.portieri[index] = null;
								}
							});
							break;
						case 1:
						//se la posizione selezionata non è vuota, tolgo inFormazione dal giocatore nella rosa
							if(arrayGiocatori[indice.value] != null){
								$scope.vm.difensoriRosa.forEach(function(item, index){								
									if(arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE){
										$scope.vm.difensoriRosa[index].inFormazione = false;
									}
								});
							}
						//cerco il giocatore in campo e in panchina, quando lo trovo, nullizzo
							$scope.vm.difensoriPanchina.forEach(function(item,index){
								if(item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE){
									$scope.vm.difensoriPanchina[index] = null;
								}
							});
							$scope.vm.difensori.forEach(function(item,index){
								if(item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE){
									$scope.vm.difensori[index] = null;
								}
							});
							break;
						case 2:
						//se la posizione selezionata non è vuota, tolgo inFormazione dal giocatore nella rosa
							if(arrayGiocatori[indice.value] != null){
								$scope.vm.centrocampistiRosa.forEach(function(item, index){								
									if(arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE){
										$scope.vm.centrocampistiRosa[index].inFormazione = false;
									}
								});
							}
						//cerco il giocatore in campo e in panchina, quando lo trovo, nullizzo
							$scope.vm.centrocampistiPanchina.forEach(function(item,index){
								if(item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE){
									$scope.vm.centrocampistiPanchina[index] = null;
								}
							});
							$scope.vm.centrocampisti.forEach(function(item,index){
								if(item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE){
									$scope.vm.centrocampisti[index] = null;
								}
							});
							break;
						case 3:
						//se la posizione selezionata non è vuota, tolgo inFormazione dal giocatore nella rosa
							if(arrayGiocatori[indice.value] != null){
								$scope.vm.attaccantiRosa.forEach(function(item, index){								
									if(arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE){
										$scope.vm.attaccantiRosa[index].inFormazione = false;
									}
								});
							}
						//cerco il giocatore in campo e in panchina, quando lo trovo, nullizzo
							$scope.vm.attaccantiPanchina.forEach(function(item,index){
								if(item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE){
									$scope.vm.attaccantiPanchina[index] = null;
								}
							});
							$scope.vm.attaccanti.forEach(function(item,index){
								if(item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE){
									$scope.vm.attaccanti[index] = null;
								}
							});
							break;
						case 4:
						//se la posizione selezionata non è vuota, tolgo inFormazione dal giocatore nella rosa
							if(arrayGiocatori[indice.value] != null){
								$scope.vm.allenatoriRosa.forEach(function(item, index){								
									if(arrayGiocatori[indice.value].ID_GIOCATORE == item.ID_GIOCATORE){
										$scope.vm.allenatoriRosa[index].inFormazione = false;
									}
								});
							}
						//cerco il giocatore in campo e in panchina, quando lo trovo, nullizzo
							$scope.vm.allenatoriPanchina.forEach(function(item,index){
								if(item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE){
									$scope.vm.allenatoriPanchina[index] = null;
								}
							});
							$scope.vm.allenatori.forEach(function(item,index){
								if(item && item.ID_GIOCATORE == giocatore.ID_GIOCATORE){
									$scope.vm.allenatori[index] = null;
								}
							});
							break;
					};
				}
				arrayGiocatori[indice.value] = giocatore;
			}

			if(indice.value == arrayGiocatori.length-1)
				indice.value = 0;
			else
				indice.value++;
		};

		function _openRosaPanchina(ruolo, ruoloPosition, indice, e){
			$scope.vm.portieriPanchinaPosition.value = null;
			$scope.vm.difensoriPanchinaPosition.value = null;
			$scope.vm.centrocampistiPanchinaPosition.value = null;
			$scope.vm.attaccantiPanchinaPosition.value = null;
			ruoloPosition.value = indice;
			$scope.vm.rosaPanchinaShown = ruolo;
			$scope.popoverPanchinaRosa.show(e);
		};

		$scope.firstLetter = function(text: string){
			if (text && text.trim())
				return text.charAt(0)+'.';
			else
				return '';
		};

		$scope.popPanchina = function(e){
			e.stopPropagation();
			$scope.vm.rosaShown.ruolo = '';
			$scope.vm.rosaShown.position = null;
			$scope.popoverPanchina.show(e);
		};

		function _popTattiche(e){
			e.stopPropagation();
			$scope.vm.rosaShown.ruolo = '';
			$scope.vm.rosaShown.position = null;
			$scope.popoverTattiche.show(e);
		};

		$scope.ruoloGiocatoreSelected = function(){
			if($scope.vm.rosaPanchinaShown == 'P'){
				return $scope.vm.portieriRosa.length;
			}
			if($scope.vm.rosaPanchinaShown == 'D'){
				return $scope.vm.difensoriRosa.length;
			}
			if($scope.vm.rosaPanchinaShown == 'C'){
				return $scope.vm.centrocampistiRosa.length;
			}
			if($scope.vm.rosaPanchinaShown == 'A'){
				return $scope.vm.attaccantiRosa.length;
			}
			if($scope.vm.rosaPanchinaShown == 'AL'){
				return $scope.vm.allenatoriRosa.length;
			}
		};

		$scope.saveFormazione = function(){
			$rootScope.showLoading();

			var formazioneValida = true;
			var primoBuco = -1

			var formazione = [].concat($scope.vm.portieri, $scope.vm.difensori, $scope.vm.centrocampisti, $scope.vm.attaccanti, $scope.vm.portieriPanchina, 
				$scope.vm.difensoriPanchina, $scope.vm.centrocampistiPanchina, $scope.vm.attaccantiPanchina);
			if($rootScope.misterCalcioCup){
				formazione = formazione.concat($scope.vm.allenatori, $scope.vm.allenatoriPanchina);
				if($scope.vm.allenatori[0] == null){
					formazioneValida = false;
					primoBuco = 1;
				} else if ($scope.vm.allenatoriPanchina[0] == null){
					formazioneValida = false;
					primoBuco = 11;
				}
			}
			var add = '';;
			for(var i = 0; i < formazione.length && formazioneValida == true; i++){
				if(formazione[i] != null){
					add += formazione[i].ID_GIOCATORE + ':' + i + ';';
				} else {
					formazioneValida = false;
					primoBuco = i;
				}
			}
			if(formazioneValida == false){
				if(primoBuco <= 10){
					$rootScope.hideLoading();
        			$rootScope.showToast('Formazione incompleta. Schierare tutti i giocatori in campo');
				} else {
					$rootScope.hideLoading();
        			$rootScope.showToast('Formazione incompleta. Schierare tutti i giocatori in panchina');
				}
			} else {
				if($scope.vm.formazione == '' || $scope.vm.formazione == add){
					FantaCalcioServices.salvaFormazione($rootScope.modoGioco, $rootScope.squadraSelected.codice_squadra, $scope.vm.tatticaSelected.TATTICA, 
						add.substr(0, add.length-1))
					.then(function(data){
						if(data.success){
							if (window.cordova) {
								AdMob.showInterstitial(function () {
									AdMob.prepareInterstitial({
										adId: $rootScope.admobid.interstitial,
										autoShow: false
									});
									$rootScope.showToast(data.success.msg_utente);
								}, function () {
									AdMob.prepareInterstitial({
										adId: $rootScope.admobid.interstitial,
										autoShow: false
									});
									$rootScope.showToast(data.success.msg_utente);
								});
							}
							$scope.vm.formazione = add;
						} else {
        					$rootScope.showToast(data.error.user_message);
	                    if(data.error && data.error.id == 'not_logged')
	                        $rootScope.autologin();
	                    }
						$rootScope.hideLoading();
					}, function(data){
						$rootScope.hideLoading();
    					$rootScope.showToast('Errore nella connessione');
					});
				} else {
					var remove = '';
					var addSplit = add.split(';');
					add = '';
					var formazioneSplit = $scope.vm.formazione.split(';');
					for(var i = 0; i < addSplit.length - 1; i++){
						if(addSplit[i] != formazioneSplit[i]){
							var giocatoreRimosso = formazioneSplit[i].split(':');
							var giocatoreModificato = addSplit[i].split(':');
							remove += giocatoreRimosso[0] + ';';
							add += giocatoreModificato[0] + ':' + i + ';';
						}
					}
					remove = remove.substring(0, remove.length - 1);
					add = add.substring(0, add.length - 1);
					console.log("Remove e add prima della chiamata");
					console.log(remove);
					console.log(add);
					FantaCalcioServices.modificaFormazione($rootScope.modoGioco, $rootScope.squadraSelected.codice_squadra, $scope.vm.tatticaSelected.TATTICA, 
						remove, add).then(function(data){
							if (data.success) {
								if (window.cordova) {
									AdMob.showInterstitial(function () {
										AdMob.prepareInterstitial({
											adId: $rootScope.admobid.interstitial,
											autoShow: false
										});
										$rootScope.showToast(data.success.msg_utente);
									}, function () {
										AdMob.prepareInterstitial({
											adId: $rootScope.admobid.interstitial,
											autoShow: false
										});
										$rootScope.showToast(data.success.msg_utente);
									});
								}
								$scope.vm.formazione = addSplit.join(';');
							} else {
    							$rootScope.showToast(data.error.user_message);
							}
							$rootScope.hideLoading();
						},function(data){
							$rootScope.hideLoading();
							$rootScope.showToast('Errore nella connessione');
						});

				}
			}
			//$rootScope.hideLoading();
		};

		$scope.deleteFormazione = function(){
			$rootScope.showLoading();
			FantaCalcioServices.cancellaFormazione($rootScope.modoGioco, $rootScope.squadraSelected.codice_squadra, $scope.vm.tatticaSelected.TATTICA)
			.then(function(data){
				if(data.success){
					$scope.vm.portieri.forEach(function(item,index){
						$scope.vm.portieri[index] = null;
					});
					$scope.vm.portieriPanchina.forEach(function(item,index){
						$scope.vm.portieriPanchina[index] = null;
					});
					$scope.vm.portieriRosa.forEach(function(item){
						item.inFormazione = false;
					});
					$scope.vm.difensori.forEach(function(item,index){
						$scope.vm.difensori[index] = null;
					});
					$scope.vm.difensoriPanchina.forEach(function(item,index){
						$scope.vm.difensoriPanchina[index] = null;
					});
					$scope.vm.difensoriRosa.forEach(function(item){
						item.inFormazione = false;
					});
					$scope.vm.centrocampisti.forEach(function(item,index){
						$scope.vm.centrocampisti[index] = null;
					});
					$scope.vm.centrocampistiPanchina.forEach(function(item,index){
						$scope.vm.centrocampistiPanchina[index] = null;
					});
					$scope.vm.centrocampistiRosa.forEach(function(item){
						item.inFormazione = false;
					});
					$scope.vm.attaccanti.forEach(function(item,index){
						$scope.vm.attaccanti[index] = null;
					});
					$scope.vm.attaccantiPanchina.forEach(function(item,index){
						$scope.vm.attaccantiPanchina[index] = null;
					});
					$scope.vm.attaccantiRosa.forEach(function(item){
						item.inFormazione = false;
					});
					if($rootScope.misterCalcioCup){
						$scope.vm.allenatori.forEach(function(item,index){
							$scope.vm.allenatori[index] = null;
						});
						$scope.vm.allenatoriPanchina.forEach(function(item,index){
							$scope.vm.allenatoriPanchina[index] = null;
						});
						$scope.vm.allenatoriRosa.forEach(function(item){
							item.inFormazione = false;
						});
					}
					$scope.vm.rosaShown = {ruolo:'', position: null};
					$scope.vm.formazione = '';

					$rootScope.showToast(data.success.msg_utente);
				} else {
					$rootScope.showToast(data.error.user_message);
				}
				$rootScope.hideLoading();
			},function(data){
				$rootScope.hideLoading();
				$rootScope.showToast('Errore nella connessione');
			});
		};

		$ionicPopover.fromTemplateUrl('popoverTattiche.html', {
			scope: $scope,
		}).then(function(popover) {
			$scope.popoverTattiche = popover;
		});

		$ionicPopover.fromTemplateUrl('popoverPanchina.html', {
			scope: $scope,
		}).then(function(popover) {
			$scope.popoverPanchina = popover;
		});

		$ionicPopover.fromTemplateUrl('popoverPanchinaRosa.html', {
			scope: $scope,
		}).then(function(popover) {
			$scope.popoverPanchinaRosa = popover;
		});

		$scope.$on('$destroy', function() {
			$scope.popoverTattiche.remove();
			$scope.popoverPanchina.remove();
			$scope.popoverPanchinaRosa.remove();
		});

	});
	
	function getGiocatoreById(rosa: OM.RosaFormazioneData[], idGiocatore: number) {
		var ris: OM.RosaFormazioneData = null;
		rosa.some(function(item){
			if(item.ID_GIOCATORE == idGiocatore){
				ris = item;
				return true;
			}
		});
		if(ris == null) {
			ris = {
				CODICE_RUOLO: -1,
				COGNOME: '',
				ID_GIOCATORE: -1,
				IN_ROSA: 0,
				NOME: '',
				SQUADRA: '',
				inFormazione: false
			};
		}
		return ris;
	};

})();
