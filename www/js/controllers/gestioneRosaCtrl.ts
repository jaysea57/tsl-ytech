/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
	var app = angular.module('gestioneRosaCtrl', []);
	app.controller('gestioneRosaCtrl', function ($scope, $rootScope, $state, $ionicLoading, $stateParams, $ionicHistory, $ionicModal, $timeout,
		$location, $ionicScrollDelegate, $cordovaGoogleAnalytics,
		FantaCalcioServices: OM.FantaCalcioFactory, RosaService: OM.RosaService) {
		 
		$scope.vm = {
			portieriRosa: [],
			totalePortieri: 0,
			difensoriRosa: [],
			totaleDifensori: 0,
			centrocampistiRosa: [],
			totaleCentrocampisti: 0,
			attaccantiRosa: [],
			totaleAttaccanti: 0,
			allenatoriRosa: [],
			totaleAllenatori: 0,
			rosaShown: '',
			indiceShown: -1,
			saldo: 0,
			venduti: '',
			acquistati: '',
			giocatoriDisponibili: [],
			squadreFiltro: [],
			//variabili usate solo per la visualizzazione
			//bloccoRosa: false,
			totaleRosa: 0,
			portieriLength: 0,
			difensoriLength: 0,
			centrocampistiLength: 0,
			attaccantiLength: 0,
			allenatoriLength: 0,
			totaleCosto: 0
		};			
		$scope.filter = {
			ordinaPerNome: true,
			ordinaCrescente: true,
			filtroSquadra: [],
			minCosto: 1,
			maxCosto: 46,
		}

		
		function _vendiGiocatore(giocatore: OM.ROSA){
			var prezzoVendita;
			if(RosaService.getOfferte() == true)
				prezzoVendita = giocatore.COSTO_ACQUISTO;
			else
				prezzoVendita = giocatore.VALORE_ATTUALE;	
			var confirmText = "Sei sicuro di voler vendere " + giocatore.COGNOME + " al prezzo di " + prezzoVendita + " crediti?";
			if(window.cordova){
				navigator.notification.confirm(confirmText, function(buttonPressed:number){
					if(buttonPressed == 1){
						venditaConfermata(giocatore, prezzoVendita);
						$scope.$apply();
					}
				}, 'Vendi giocatore', ['Conferma', 'Annulla']);
			} else {
				if(confirm(confirmText)){
					venditaConfermata(giocatore, prezzoVendita);
				}
			}
		};

		function venditaConfermata(giocatore, prezzoVendita: number){
			switch(giocatore.RUOLO){
					case 0:
						$scope.vm.portieriRosa[$scope.vm.portieriRosa.indexOf(giocatore)] = {RUOLO: 0};
						if($scope.vm.venduti.indexOf(giocatore.ID) == -1)
							$scope.vm.venduti += giocatore.ID + ';'
						if($scope.vm.acquistati.indexOf(giocatore.ID) > -1){
							var stringaAcquistati: any = giocatore.ID;
							if(RosaService.getOfferte()){
								stringaAcquistati += ':' + prezzoVendita;
							}
							var splitted = $scope.vm.acquistati.split(";");
							splitted.splice(splitted.indexOf(String(stringaAcquistati)),1);
							$scope.vm.acquistati = splitted.join(";");
							RosaService.setGiocatoriAcquistati($scope.vm.acquistati);
						}
						$scope.vm.saldo += prezzoVendita;
						RosaService.setGiocatoriVenduti($scope.vm.venduti);
						RosaService.giocatoreRemovedFromRosa(giocatore);
						break;
					case 1:
						$scope.vm.difensoriRosa[$scope.vm.difensoriRosa.indexOf(giocatore)] = {RUOLO: 1};
						if($scope.vm.venduti.indexOf(giocatore.ID) == -1)
							$scope.vm.venduti += giocatore.ID + ';'
						if($scope.vm.acquistati.indexOf(giocatore.ID) > -1){
							var stringaAcquistati: any = giocatore.ID;
							if(RosaService.getOfferte()){
								stringaAcquistati += ':' + prezzoVendita;
							}
							var splitted = $scope.vm.acquistati.split(";");
							splitted.splice(splitted.indexOf(String(stringaAcquistati)),1);
							$scope.vm.acquistati = splitted.join(";");
							RosaService.setGiocatoriAcquistati($scope.vm.acquistati);
						}
						$scope.vm.saldo += prezzoVendita;
						RosaService.setGiocatoriVenduti($scope.vm.venduti);
						RosaService.giocatoreRemovedFromRosa(giocatore);
						break;
					case 2:
						$scope.vm.centrocampistiRosa[$scope.vm.centrocampistiRosa.indexOf(giocatore)] = {RUOLO: 2};
						if($scope.vm.venduti.indexOf(giocatore.ID) == -1)
							$scope.vm.venduti += giocatore.ID + ';'
						if($scope.vm.acquistati.indexOf(giocatore.ID) > -1){
							var stringaAcquistati: any = giocatore.ID;
							if(RosaService.getOfferte()){
								stringaAcquistati += ':' + prezzoVendita;
							}
							var splitted = $scope.vm.acquistati.split(";");
							splitted.splice(splitted.indexOf(String(stringaAcquistati)),1);
							$scope.vm.acquistati = splitted.join(";");
							RosaService.setGiocatoriAcquistati($scope.vm.acquistati);
						}
						$scope.vm.saldo += prezzoVendita;
						RosaService.setGiocatoriVenduti($scope.vm.venduti);
						RosaService.giocatoreRemovedFromRosa(giocatore);
						break;
					case 3:
						$scope.vm.attaccantiRosa[$scope.vm.attaccantiRosa.indexOf(giocatore)] = {RUOLO: 3};
						if($scope.vm.venduti.indexOf(giocatore.ID) == -1)
							$scope.vm.venduti += giocatore.ID + ';'
						if($scope.vm.acquistati.indexOf(giocatore.ID) > -1){
							var stringaAcquistati: any = giocatore.ID;
							if(RosaService.getOfferte()){
								stringaAcquistati += ':' + prezzoVendita;
							}
							var splitted = $scope.vm.acquistati.split(";");
							splitted.splice(splitted.indexOf(String(stringaAcquistati)),1);
							$scope.vm.acquistati = splitted.join(";");
							RosaService.setGiocatoriAcquistati($scope.vm.acquistati);
						}
						$scope.vm.saldo += prezzoVendita;
						RosaService.setGiocatoriVenduti($scope.vm.venduti);
						RosaService.giocatoreRemovedFromRosa(giocatore);
						break;
					case 4:
						$scope.vm.allenatoriRosa[$scope.vm.allenatoriRosa.indexOf(giocatore)] = {RUOLO: 4};
						if($scope.vm.venduti.indexOf(giocatore.ID) == -1)
							$scope.vm.venduti += giocatore.ID + ';'
						if($scope.vm.acquistati.indexOf(giocatore.ID) > -1){
							var stringaAcquistati: any = giocatore.ID;
							if(RosaService.getOfferte()){
								stringaAcquistati += ':' + prezzoVendita;
							}
							var splitted = $scope.vm.acquistati.split(";");
							splitted.splice(splitted.indexOf(String(stringaAcquistati)),1);
							$scope.vm.acquistati = splitted.join(";");
							RosaService.setGiocatoriAcquistati($scope.vm.acquistati);
						}
						$scope.vm.saldo += prezzoVendita;
						RosaService.setGiocatoriVenduti($scope.vm.venduti);
						RosaService.giocatoreRemovedFromRosa(giocatore);
						break;
				};
				$scope.totaleRosa();
				$scope.vm.portieriLength = _lengthEffettiva($scope.vm.portieriRosa);
				$scope.vm.difensoriLength = _lengthEffettiva($scope.vm.difensoriRosa);
				$scope.vm.centrocampistiLength = _lengthEffettiva($scope.vm.centrocampistiRosa);
				$scope.vm.attaccantiLength = _lengthEffettiva($scope.vm.attaccantiRosa);
				$scope.vm.allenatoriLength = _lengthEffettiva($scope.vm.allenatoriRosa);
				$scope.vm.totaleCosto = $scope.totaleCosto();
		};

		$scope.totaleRosa = function(){
			if($rootScope.misterCalcioCup)
				$scope.vm.totaleRosa = _lengthEffettiva($scope.vm.portieriRosa) + _lengthEffettiva($scope.vm.difensoriRosa) + 
					_lengthEffettiva($scope.vm.centrocampistiRosa) + _lengthEffettiva($scope.vm.attaccantiRosa)
					+ _lengthEffettiva($scope.vm.allenatoriRosa);
			else
				$scope.vm.totaleRosa = _lengthEffettiva($scope.vm.portieriRosa) + _lengthEffettiva($scope.vm.difensoriRosa) + 
					_lengthEffettiva($scope.vm.centrocampistiRosa) + _lengthEffettiva($scope.vm.attaccantiRosa);
		};

		$scope.totaleCosto = function(){
			var totale = 0;
			$scope.vm.portieriRosa.forEach(function(item:OM.ROSA){
				if(item.COGNOME)
					totale += item.COSTO_ACQUISTO;
			});
			$scope.vm.difensoriRosa.forEach(function(item:OM.ROSA){
				if(item.COGNOME)
					totale += item.COSTO_ACQUISTO;
			});
			$scope.vm.centrocampistiRosa.forEach(function(item:OM.ROSA){
				if(item.COGNOME)
					totale += item.COSTO_ACQUISTO;
			});
			$scope.vm.attaccantiRosa.forEach(function(item:OM.ROSA){
				if(item.COGNOME)
					totale += item.COSTO_ACQUISTO;
			});
			if($rootScope.misterCalcioCup){
				$scope.vm.allenatoriRosa.forEach(function(item:OM.ROSA){
					if(item.COGNOME)
						totale += item.COSTO_ACQUISTO;
				});
			}
			return totale;
		};

		function _aggiungiGiocatore(ruolo, index){
			$rootScope.showLoading();
			$timeout(function(){
				RosaService.setLastSelected({ruolo: $scope.vm.rosaShown, index: index});
				if($rootScope.misterCalcioCup)
					RosaService.setRosa($scope.vm.portieriRosa, $scope.vm.difensoriRosa, $scope.vm.centrocampistiRosa, $scope.vm.attaccantiRosa, $scope.vm.allenatoriRosa);
				else
					RosaService.setRosa($scope.vm.portieriRosa, $scope.vm.difensoriRosa, $scope.vm.centrocampistiRosa, $scope.vm.attaccantiRosa);
				RosaService.setGiocatoriVenduti($scope.vm.venduti);
				RosaService.setSaldo($scope.vm.saldo);
				$state.go("app.aggiungiGiocatore", {ruoloGiocatore: ruolo});
			}, 10);
		};

		function _lengthEffettiva(array){
			if(array){
				var ris = 0;
				array.forEach(function(item){
					if(item.COGNOME)
						ris ++;
				});
				return ris;
			}
		};

		$scope.confermaRosa = function(){
			$rootScope.showLoading();
			var saldoOk = false;
			var numGiocatoriOk = false;

			if(RosaService.getOfferte()){
				if($scope.vm.saldo == 0)
					saldoOk = true;
			} else {
				saldoOk = true;
			}

			if($rootScope.misterCalcioCup){
				if($scope.vm.totaleRosa == 27)
					numGiocatoriOk = true;
			} else {
				if($scope.vm.totaleRosa == 25)
					numGiocatoriOk = true;
			}

			if(saldoOk && numGiocatoriOk){
				if(RosaService.getPrimaRosa()){
					$scope.vm.venduti = '';
				}
				FantaCalcioServices.modificaRosa($rootScope.squadraSelected.codice_squadra, $scope.vm.venduti.substr(0, $scope.vm.venduti.length -1), 
					$scope.vm.acquistati.substr(0, $scope.vm.acquistati.length -1)).then(function(data){
					if(data.error){
						$rootScope.showToast(data.error.user_message);
					} else {
						$scope.vm.venduti = '';
						$scope.vm.acquistati = '';
						RosaService.setGiocatoriVenduti('');
						RosaService.setGiocatoriAcquistati('');
						RosaService.setPrimaRosa(false);
						$rootScope.showToast(data.success.msg_utente);
					}
					$rootScope.hideLoading();
				}, function(data){
					$rootScope.hideLoading();
					$rootScope.showToast('Errore nella connessione');
				});
			} else {
				$rootScope.hideLoading();
				$timeout(function() {
			      	if(numGiocatoriOk == false){
						if($rootScope.misterCalcioCup)
							if(window.cordova)
								navigator.notification.alert('Inserire tutti i 27 giocatori', function(){}, 'Rosa');
							else
								alert('Inserire tutti i 27 giocatori');
						else
							if(window.cordova)
								navigator.notification.alert('Inserire tutti i 25 giocatori', function(){}, 'Rosa');
							else
								alert('Inserire tutti i 25 giocatori');
					} else if (saldoOk == false){
						if(window.cordova)
							navigator.notification.alert('Devi esaurire il saldo prima di confermare la rosa!', function(){}, 'Rosa');
						else
							alert('Devi esaurire il saldo prima di confermare la rosa!')
					}
			    }, 5);
				
			}
		};

		$scope.openRosa = function(ruolo){
			if($scope.vm.rosaShown == ruolo){
				$scope.vm.rosaShown = '';
			} else {
				$scope.vm.rosaShown = ruolo;
			}
			$timeout(function() {
				//$location.hash(ruolo);
				//console.log($location.hash());
				//$ionicScrollDelegate.$getByHandle('contentScroll').anchorScroll(true);
				$ionicScrollDelegate.$getByHandle('contentScroll').resize();
		  	}, 200);
		};

		$scope.firstLetter = function(text: string){
			if(text)
				return text.charAt(0)+'.';
			else
				return '';
		};

		function rosaInit(){

			if (window.cordova) $cordovaGoogleAnalytics.trackView('Gestione Rosa');

			if($rootScope.rosaInCorso == false){
				FantaCalcioServices.gestioneRosa($rootScope.modoGioco, $rootScope.squadraSelected.codice_squadra)
				.then(function(data){
					if(data.error){
						$rootScope.showToast(data.error.user_message);
					} else {
						console.log(data);
						RosaService.setGiocatoriAcquistati('');
						RosaService.setGiocatoriVenduti('');
						if($rootScope.modoGioco != 3){
							if(data.asta_conclusa == true)
								RosaService.setOfferte(false);
							else
								RosaService.setOfferte(true);
						} else {
							RosaService.setOfferte(false);
						}
						$scope.vm.giocatoriDisponibili = data.GIOCATORI_DISPONIBILI;
						RosaService.setGiocatoriDisponibili($scope.vm.giocatoriDisponibili);
						$scope.vm.saldo = data.saldo;
						if(data.ROSA.length>0){
							RosaService.setPrimaRosa(false);
							data.ROSA.forEach(function(item){
								switch(item.RUOLO){
									case 0:
										$scope.vm.portieriRosa.push(item);
										break;
									case 1:
										$scope.vm.difensoriRosa.push(item);
										break;
									case 2:
										$scope.vm.centrocampistiRosa.push(item);
										break;
									case 3:
										$scope.vm.attaccantiRosa.push(item);
										break;
									case 4:
										$scope.vm.allenatoriRosa.push(item);
										break;
								};
							});
							for (var i: number = $scope.vm.portieriRosa.length; i < 3; ++i) {
								$scope.vm.portieriRosa.push({RUOLO: 0});
							}
							for(var i: number = $scope.vm.difensoriRosa.length; i < 8; i++){
								$scope.vm.difensoriRosa.push({RUOLO: 1});
							}
							for(var i: number = $scope.vm.centrocampistiRosa.length; i < 8; i++){
								$scope.vm.centrocampistiRosa.push({RUOLO: 2});
							}
							for(var i: number = $scope.vm.attaccantiRosa.length; i < 6; i++){
								$scope.vm.attaccantiRosa.push({RUOLO: 3});
							}
							if($rootScope.misterCalcioCup){
								for(var i: number = $scope.vm.allenatoriRosa.length; i < 2; i++){
									$scope.vm.allenatoriRosa.push({RUOLO: 4});
								}
							}
						} else {
							RosaService.setPrimaRosa(true);
							for(var i: number = 0; i < 3; i++){
								$scope.vm.portieriRosa.push({RUOLO: 0});
							}
							for(var i: number = 0; i < 8; i++){
								$scope.vm.difensoriRosa.push({RUOLO: 1});
							}
							for(var i: number = 0; i < 8; i++){
								$scope.vm.centrocampistiRosa.push({RUOLO: 2});
							}
							for(var i: number = 0; i < 6; i++){
								$scope.vm.attaccantiRosa.push({RUOLO: 3});
							}
							if($rootScope.misterCalcioCup){
								for(var i: number = 0; i < 2; i++){
									$scope.vm.allenatoriRosa.push({RUOLO: 4});
								}
							}
						}
						$rootScope.showToast(data.msg_blocco_rosa, 5000);
	                    if(data.error && data.error.id == 'not_logged'){
	                        $rootScope.autologin();
						}
					}
					if(data.bloccoRosa || data.campDone){
						RosaService.setBloccoRosa(true);
						$scope.vm.bloccoRosa = true;						
					} else {
						RosaService.setBloccoRosa(false);
						$scope.vm.bloccoRosa = false;
						$scope.aggiungiGiocatore = _aggiungiGiocatore;
						$scope.vendiGiocatore = _vendiGiocatore;
					}
					$scope.totaleRosa();
					$scope.vm.portieriLength = _lengthEffettiva($scope.vm.portieriRosa);
					$scope.vm.difensoriLength = _lengthEffettiva($scope.vm.difensoriRosa);
					$scope.vm.centrocampistiLength = _lengthEffettiva($scope.vm.centrocampistiRosa);
					$scope.vm.attaccantiLength = _lengthEffettiva($scope.vm.attaccantiRosa);
					$scope.vm.allenatoriLength = _lengthEffettiva($scope.vm.allenatoriRosa);
					$scope.vm.totaleCosto = $scope.totaleCosto();

					
					$rootScope.hideLoading();
				},function(data){
					$rootScope.hideLoading();
					$rootScope.showToast('Errore nella connessione');
				});
			} else {
				RosaService.getRosa().forEach(function(item){
					switch(item.RUOLO){
						case 0:
							$scope.vm.portieriRosa.push(item);
							break;
						case 1:
							$scope.vm.difensoriRosa.push(item);
							break;
						case 2:
							$scope.vm.centrocampistiRosa.push(item);
							break;
						case 3:
							$scope.vm.attaccantiRosa.push(item);
							break;
						case 4:
							$scope.vm.allenatoriRosa.push(item);
							break;
					};
				});
				$scope.vm.venduti = RosaService.getGiocatoriVenduti();
				$scope.vm.acquistati = RosaService.getGiocatoriAcquistati();
				$scope.vm.rosaShown = RosaService.getLastSelected().ruolo;
				$scope.vm.indiceShown = RosaService.getLastSelected().index;
				$scope.vm.giocatoriDisponibili = RosaService.getGiocatoriDisponibili();
				$scope.vm.saldo = RosaService.getSaldo();
				if(RosaService.getBloccoRosa()){
					$scope.vm.bloccoRosa = true;						
				} else {
					$scope.vm.bloccoRosa = false;
					$scope.aggiungiGiocatore = _aggiungiGiocatore;
					$scope.vendiGiocatore = _vendiGiocatore;
				}
				$scope.totaleRosa();
				$scope.vm.portieriLength = _lengthEffettiva($scope.vm.portieriRosa);
				$scope.vm.difensoriLength = _lengthEffettiva($scope.vm.difensoriRosa);
				$scope.vm.centrocampistiLength = _lengthEffettiva($scope.vm.centrocampistiRosa);
				$scope.vm.attaccantiLength = _lengthEffettiva($scope.vm.attaccantiRosa);
				$scope.vm.allenatoriLength = _lengthEffettiva($scope.vm.allenatoriRosa);
				$scope.vm.totaleCosto = $scope.totaleCosto();
				$rootScope.hideLoading();
			}
		};

		$scope.$on('$ionicView.beforeEnter', rosaInit());

		$scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
			if(toState.name == 'app.aggiungiGiocatore'){
				$rootScope.rosaInCorso = true;
			} else {
				$rootScope.rosaInCorso = false;
			}
		});

	});

	function isInteger(x) {
		return x % 1 === 0;
	}

})();
