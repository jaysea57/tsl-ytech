/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function(){
	var app = angular.module('appCtrl', []);

	app.controller('AppCtrl', function($scope, $rootScope, $state, $ionicHistory, $cordovaFileTransfer, $timeout,
		FantaCalcioServices: OM.FantaCalcioFactory, $ionicLoading) {
		
		$rootScope.hideNews= {value: false};
		if(!$rootScope.secondaLogin){
			$rootScope.squadreTorneoClassico = [];
			$rootScope.squadreTorneoGironi = [];			
		}
/*
		if($rootScope.misterCalcioCup){
			if(!$rootScope.secondaLogin){
				$rootScope.userClassico = JSON.parse(window.localStorage.getItem('misterCalcioCupUserTorneoClassico')) || '';
				$rootScope.userGironi = JSON.parse(window.localStorage.getItem('misterCalcioCupUserTorneoGironi')) || '';
				//$rootScope.modoGioco = JSON.parse(window.localStorage.getItem('misterCalcioCupModoGioco')) || '';
				if($rootScope.modoGioco == 3){
					$rootScope.user = $rootScope.userClassico;
				} else {
					$rootScope.user = $rootScope.userGironi;
				}
				$rootScope.squadraSelected = JSON.parse(window.localStorage.getItem('misterCalcioCupSquadraSelected'+$rootScope.user.idCliente)) || '';
				$rootScope.squadreTorneoClassico = JSON.parse(window.localStorage.getItem('misterCalcioCupSquadreTorneoClassico'+$rootScope.user.idCliente)) || [];
				$rootScope.squadreTorneoGironi = JSON.parse(window.localStorage.getItem('misterCalcioCupSquadreTorneoGironi'+$rootScope.user.idCliente)) || [];
			}
		} else {
			if(!$rootScope.secondaLogin){
				$rootScope.userClassico = JSON.parse(window.localStorage.getItem('tuttoSportLeagueUserTorneoClassico')) || '';
				$rootScope.userGironi = JSON.parse(window.localStorage.getItem('tuttoSportLeagueUserTorneoGironi')) || '';
				//$rootScope.modoGioco = JSON.parse(window.localStorage.getItem('tuttoSportLeagueModoGioco')) || '';
				if($rootScope.modoGioco == 3){
					$rootScope.user = $rootScope.userClassico;
				} else {
					$rootScope.user = $rootScope.userGironi;
				}
				$rootScope.squadraSelected = JSON.parse(window.localStorage.getItem('tuttoSportLeagueSquadraSelected'+$rootScope.user.idCliente)) || '';
				$rootScope.squadreTorneoClassico = JSON.parse(window.localStorage.getItem('tuttoSportLeagueSquadreTorneoClassico'+$rootScope.user.idCliente)) || [];
				$rootScope.squadreTorneoGironi = JSON.parse(window.localStorage.getItem('tuttoSportLeagueSquadreTorneoGironi'+$rootScope.user.idCliente)) || [];
			}
		}
	*/

		$scope.accordionMenu = false;
		$scope.openAccordion = function(){
			if($scope.accordionMenu == true)
				$scope.accordionMenu = false;
			else
				$scope.accordionMenu = true;
		};

		$scope.selectSquadra = function(squadra, modo){
			$rootScope.squadraSelected = squadra;
			$rootScope.modoGioco = modo;
			if(modo == 3)
				$rootScope.user = $rootScope.userClassico;
			else
				$rootScope.user = $rootScope.userGironi;
			if($rootScope.squadraSelected.playoff){
				$rootScope.modoGioco = 4;
				if($rootScope.misterCalcioCup)
					window.localStorage.setItem('misterCalcioCupModoGioco', $rootScope.modoGioco);
				else
					window.localStorage.setItem('tuttoSportLeagueModoGioco', $rootScope.modoGioco);
			}
			if($rootScope.misterCalcioCup)
				window.localStorage.setItem('misterCalcioCupSquadraSelected'+$rootScope.user.idCliente,JSON.stringify(squadra));
			else
				window.localStorage.setItem('tuttoSportLeagueSquadraSelected'+$rootScope.user.idCliente,JSON.stringify(squadra));
			$scope.accordionMenu = false;
			$ionicHistory.nextViewOptions({
				disableAnimate: true,
				disableBack: true,
				historyRoot: true
			});
			$rootScope.showToast($rootScope.squadraSelected.nome);
			$state.go('app.home');
		};

		$scope.nuovaSquadra = function(text, _modoGioco){
			if(window.cordova){
				navigator.notification.confirm('Vuoi creare una nuova squadra ed iscriverla al torneo ' + text, 
					function(buttonIndex?: number){
						if(buttonIndex == 1){
							$rootScope.showLoading();
							if(_modoGioco == 3){
								$rootScope.user = $rootScope.userClassico;
							} else {
								$rootScope.user = $rootScope.userGironi;
							}
							FantaCalcioServices.nuovaSquadra($rootScope.user.idCliente, _modoGioco).then(function(data){
								if(data.error){
									$rootScope.hideLoading();
									$rootScope.showToast(data.error.user_message);
								} else {
									var nuovaSquadra: OM.Squadre;
									FantaCalcioServices.getSquadre($rootScope.user.idCliente, _modoGioco).then(function(dataSquadre){
										dataSquadre.squadre.forEach(function(item){
											if(data.squadre[0].codice_squadra == item.codice_squadra)
												nuovaSquadra = item;
										});
										$rootScope.squadraSelected = nuovaSquadra;
										$rootScope.modoGioco = _modoGioco;
										if($rootScope.misterCalcioCup){
											window.localStorage.setItem('misterCalcioCupSquadraSelected'+$rootScope.user.idCliente, JSON.stringify($rootScope.squadraSelected));
											if(_modoGioco == 3){
												$rootScope.squadreTorneoClassico.push(nuovaSquadra);
												window.localStorage.setItem('misterCalcioCupSquadreTorneoClassico'+$rootScope.user.idCliente, JSON.stringify($rootScope.squadreTorneoClassico));
											}
											else
												$rootScope.squadreTorneoGironi.push(nuovaSquadra);
												window.localStorage.setItem('misterCalcioCupSquadreTorneoGironi'+$rootScope.user.idCliente, JSON.stringify($rootScope.squadreTorneoGironi));
										} else {
											window.localStorage.setItem('tuttoSportLeagueSquadraSelected'+$rootScope.user.idCliente,JSON.stringify($rootScope.squadraSelected));
											if(_modoGioco == 3){
												$rootScope.squadreTorneoClassico.push(nuovaSquadra);
												window.localStorage.setItem('tuttoSportLeagueSquadreTorneoClassico'+$rootScope.user.idCliente, JSON.stringify($rootScope.squadreTorneoClassico));
											} else {
												$rootScope.squadreTorneoGironi.push(nuovaSquadra);
												window.localStorage.setItem('tuttoSportLeagueSquadreTorneoGironi'+$rootScope.user.idCliente, JSON.stringify($rootScope.squadreTorneoGironi));
											}
										}
										$scope.accordionMenu = false;
										$ionicHistory.nextViewOptions({
											disableAnimate: false,
											disableBack: true,
											historyRoot: false
										});
										$state.go('app.opzioniSquadra', {nuovaSquadra: 1});
									},function(dataSquadre){
										$rootScope.hideLoading();
										$rootScope.showToast('Errore di connessione');
									});
								}
							}, function(data){
								$rootScope.hideLoading();
								$rootScope.showToast('Errore di connessione');
							});
						}
					}, 
					'Aggiungi squadra', ['Conferma', 'Annulla']);
			}
		};

/*
		function _addSquadra(buttonIndex?: number){
			if(buttonIndex == 1){
				$rootScope.showLoading();
				FantaCalcioServices.nuovaSquadra($rootScope.user.idCliente, $rootScope.modoGioco).then(function(data){
					if(data.error){
						$rootScope.hideLoading();
						$rootScope.showToast(data.error.user_message);
					} else {
						var nuovaSquadra: OM.Squadre;
						FantaCalcioServices.getSquadre($rootScope.user.idCliente, $rootScope.modoGioco).then(function(dataSquadre){
							dataSquadre.squadre.forEach(function(item){
								if(data.squadre[0].codice_squadra == item.codice_squadra)
									nuovaSquadra = item;
							});
							$rootScope.squadraSelected = nuovaSquadra;
							if($rootScope.misterCalcioCup){
								window.localStorage.setItem('misterCalcioCupSquadraSelected'+$rootScope.user.idCliente, JSON.stringify($rootScope.squadraSelected));
								if($rootScope.modoGioco == 3){
									$rootScope.squadreTorneoClassico.push(nuovaSquadra);
									window.localStorage.setItem('misterCalcioCupSquadreTorneoClassico'+$rootScope.user.idCliente, JSON.stringify($rootScope.squadreTorneoClassico));
								}
								else
									$rootScope.squadreTorneoGironi.push(nuovaSquadra);
									window.localStorage.setItem('misterCalcioCupSquadreTorneoGironi'+$rootScope.user.idCliente, JSON.stringify($rootScope.squadreTorneoGironi));
							} else {
								window.localStorage.setItem('tuttoSportLeagueSquadraSelected'+$rootScope.user.idCliente,JSON.stringify($rootScope.squadraSelected));
								if($rootScope.modoGioco == 3){
									$rootScope.squadreTorneoClassico.push(nuovaSquadra);
									window.localStorage.setItem('tuttoSportLeagueSquadreTorneoClassico'+$rootScope.user.idCliente, JSON.stringify($rootScope.squadreTorneoClassico));
								} else {
									$rootScope.squadreTorneoGironi.push(nuovaSquadra);
									window.localStorage.setItem('tuttoSportLeagueSquadreTorneoGironi'+$rootScope.user.idCliente, JSON.stringify($rootScope.squadreTorneoGironi));
								}
							}
							$scope.accordionMenu = false;
							$ionicHistory.nextViewOptions({
								disableAnimate: false,
								disableBack: true,
								historyRoot: false
							});
							$state.go('app.opzioniSquadra', {nuovaSquadra: 1});
						},function(dataSquadre){
							$rootScope.hideLoading();
							$rootScope.showToast('Errore di connessione');
						});
					}
				}, function(data){
					$rootScope.hideLoading();
					$rootScope.showToast('Errore di connessione');
				});
			}
		}
*/

		$scope.logout = function(){
			$rootScope.showLoading();
			if($rootScope.misterCalcioCup){
				window.localStorage.removeItem('misterCalcioCupUserLogin');
				window.localStorage.removeItem('misterCalcioCupUserLoginClassico');
				window.localStorage.removeItem('misterCalcioCupUserLoginGironi');
			} else {
				window.localStorage.removeItem('tuttoSportLeagueUserLogin');
				window.localStorage.removeItem('tuttoSportLeagueUserLoginClassico');
				window.localStorage.removeItem('tuttoSportLeagueUserLoginGironi');
			}
			var modoLogout;
			var idLogout;
			if($rootScope.userClassico){
				modoLogout = 3;
				idLogout = $rootScope.userClassico.idCliente;
			} else {
				modoLogout = $rootScope.modoGioco;
				idLogout = $rootScope.userGironi.idCliente;
			}
			FantaCalcioServices.logout(idLogout, modoLogout).then(function(data){
				if(data.error){
					$rootScope.showToast(data.error.user_message);
				$rootScope.user = null;
				$rootScope.userClassico = null;
				$rootScope.userGironi = null;
				$rootScope.modoGioco = null;
				$rootScope.squadraSelected = null;
				$rootScope.squadreTorneoClassico = [];
				$rootScope.squadreTorneoGironi = [];
				$rootScope.secondaLogin = false;
				$scope.accordionMenu = false;
				$ionicHistory.nextViewOptions({
					disableAnimate: true,
					disableBack: true,
					historyRoot: true
					});
				$state.go('tipoGioco');
				} else {
					$rootScope.user = null;
					$rootScope.userClassico = null;
					$rootScope.userGironi = null;
					$rootScope.modoGioco = null;
					$rootScope.squadraSelected = null;
					$rootScope.squadreTorneoClassico = [];
					$rootScope.squadreTorneoGironi = [];
					$rootScope.secondaLogin = false;
					$scope.accordionMenu = false;
					$ionicHistory.nextViewOptions({
						disableAnimate: true,
						disableBack: true,
						historyRoot: true
					});
					$state.go('tipoGioco');
				}
			}, function(data){
				$rootScope.hideLoading();
				$rootScope.showToast('Errore di connessione');
			});
		};

		$scope.accedi = function(_modoGioco){
			$rootScope.secondaLogin = true;
			$scope.accordionMenu = false;
			$state.go('login',{gioco: _modoGioco});
		};

		$scope.goToModificaProfilo = function(user, _modoGioco){
			$rootScope.showLoading();
			$rootScope.modoGiocoPrev = $rootScope.modoGioco;
			$rootScope.modoGioco = _modoGioco;
			$rootScope.user = user;
			$scope.accordionMenu = false;
			$state.go('app.modificaProfilo');
		};

		$rootScope.autologin = function(counter: number){
			if(counter != undefined){
				if(counter == 0)
					$rootScope.showLoading();
				counter++;
				if($rootScope.misterCalcioCup)
					var loginData = JSON.parse(window.localStorage.getItem('misterCalcioCupUserLogin'));
				else
					var loginData = JSON.parse(window.localStorage.getItem('tuttoSportLeagueUserLogin'));
				FantaCalcioServices.authentication(loginData.email, loginData.password, loginData.modoGioco, $rootScope.deviceToken, $rootScope.deviceId, $rootScope.deviceTarget).then(function(data:OM.AuthenticationResponse){
					if(data.success){
						console.log(data);
						if($rootScope.misterCalcioCup){
							window.localStorage.setItem('misterCalcioCupUserLogin', JSON.stringify(loginData));
							if(loginData.modoGioco == 3){
								window.localStorage.setItem('misterCalcioCupUserTorneoClassico', JSON.stringify(data.success.user));
								$rootScope.userClassico = data.success.user;
							} else {
								window.localStorage.setItem('misterCalcioCupUserTorneoGironi', JSON.stringify(data.success.user));
								$rootScope.userGironi = data.success.user;
							}
						} else {
							window.localStorage.setItem('tuttoSportLeagueUserLogin', JSON.stringify(loginData));
							if(loginData.modoGioco == 3){
								window.localStorage.setItem('tuttoSportLeagueUserTorneoClassico', JSON.stringify(data.success.user));
								$rootScope.userClassico = data.success.user;
							} else {
								window.localStorage.setItem('tuttoSportLeagueUserTorneoGironi', JSON.stringify(data.success.user));
								$rootScope.userGironi = data.success.user;
							}
						}
						$rootScope.user = data.success.user;
						$ionicHistory.nextViewOptions({
							disableAnimate: true,
							disableBack: true,
							historyRoot: true
						});
						$state.go('app.home');
					} else if (data.error){
						console.log(data);
						if (counter <= 3) {
							$rootScope.autologin(counter);
						} else {
							if($rootScope.misterCalcioCup){
								window.localStorage.removeItem('misterCalcioCupUserLogin');
								window.localStorage.removeItem('misterCalcioCupUserLoginClassico');
								window.localStorage.removeItem('misterCalcioCupUserLoginGironi');
							} else {
								window.localStorage.removeItem('tuttoSportLeagueUserLogin');
								window.localStorage.removeItem('tuttoSportLeagueUserLoginClassico');
								window.localStorage.removeItem('tuttoSportLeagueUserLoginGironi');
							}
							$rootScope.hideLoading();
							$rootScope.showToast(data.error.user_message);
							//$timeout(function() {
							//	$ionicLoading.show({
							//		template: data.error.user_message,
							//		noBackdrop: true,
							//		duration: 2300
							//	});
							//}, 5);
						}
					}
					$rootScope.hideLoading();
				},function(data){
					$rootScope.hideLoading();
					$rootScope.showToast('Errore di connessione');
					//$timeout(function() {
					//	$ionicLoading.show({
					//		template: 'Errore nella connessione',
					//		noBackdrop: true,
					//		duration: 2300
					//	});
					//}, 5);
				});
			} else {
				$rootScope.autologin(0);
			}
		};

		$scope.goToState = function(state: string){
			if($state.current.name != state){
				$rootScope.showLoading();
				$ionicHistory.nextViewOptions({
					disableAnimate: true,
					disableBack: true,
					historyRoot: false
				});
				$state.go(state);
			};
		};

		$scope.openRegolamento = function() { // regolamento_concorso_xxx.pdf
			$rootScope.showLoading();
			/*var torneoClassicoUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
			//TUTTO SPORT LEAGUE CLASSICO
				?'http://localhost:9292/league.tsport.yland.it/'
				//:'http://league.tsport.yland.it/';     					//ESTERNA
				:'http://league.tuttosport.com/';						//PRODUZIONE
				//:'http://devts.tuttosport.com/';       						//RETE AZIENDALE Y TECH
			//MISTER CALCIO CUP CLASSICO
				//?'http://localhost:9292/gp.mr.yland.it/'
				//:'http://gp.mr.yland.it/'									//RETE ESTERNA
				//:'http://devgp.corrieredellosport.it/'                       	//RETE AZIENDALE Y TECH
				;

			var torneoGironiUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
			//TUTTO SPORT LEAGUE GIRONI modo_gioco = 1
				?'http://localhost:9292/gironi.tsport.yland.it/'
				//:'http://gironi.tsport.yland.it/';       					//ESTERNA
				:'http://gironi-league.tuttosport.com/';					//PRODUZIONE
				//:'http://devtsgironi.tuttosport.com/';       				//RETE AZIENDALE Y TECH
			//MISTER CALCIO CUP GIRONI modo_gioco = 1
				//?'http://localhost:9292/www.mr.yland.it/'
				//:'http://www.mr.yland.it/';								//ESTERNA
				//:'http://devmcc.corrieredellosport.it/'                       	//RETE AZIENDALE Y TECH
				;*/

			if($rootScope.modoGioco == 3) {
				var urlDomain = $rootScope.torneoClassicoUrl;
			} else {
				var urlDomain = $rootScope.torneoGironiUrl;
			}

			if(ionic.Platform.isIOS()){
				$rootScope.hideLoading();
				window.open(urlDomain+'doc/regolamento_concorso'+($rootScope.modoGioco==3?'_classico':'_gironi')+'.pdf',"_blank", "closebuttoncaption=Indietro,location=no,enableviewportscale=yes"); 				
			} else if(ionic.Platform.isAndroid()){
				var filePath: string = encodeURI(window.cordova.file.externalRootDirectory + ('Download/regolamento_concorso')+($rootScope.modoGioco==3?'_classico':'_gironi')+'.pdf');

			    var trustHosts = true
			    var options = {};

				$cordovaFileTransfer.download(encodeURI(urlDomain+'doc/regolamento_concorso')+($rootScope.modoGioco==3?'_classico':'_gironi')+'.pdf',
					filePath, options, trustHosts).then(function(fileEntry){
						$rootScope.hideLoading();
						console.log("Download success");
						console.log(fileEntry);
						window.cordova.plugins.fileOpener2.open(filePath, 'application/pdf');
					},
					function(error){
						$rootScope.hideLoading();
						navigator.notification.alert('Non è stato possibile recuperare l\'allegato, riprovare.', function(){}, '...');
						console.log("Download error");
						console.log(error);
					});
			}
			else {
				console.log("Platform: " + ionic.Platform.platform());
				$rootScope.hideLoading();
				if (ionic.Platform.platform() == "win32") {
					console.log("is ionic serve");
					window.open(urlDomain+'doc/regolamento_concorso'
					+($rootScope.modoGioco==3?'_classico':'_gironi')
					+'.pdf',"_blank", "closebuttoncaption=Indietro,location=no,enableviewportscale=yes");
				}
			}			
		};

		$scope.registraAPlayoff = function(){
			navigator.notification.confirm('Confermi l\'iscrizione ai playoff?', function(buttonPressed: number){
				if(buttonPressed == 1){
					FantaCalcioServices.registraAPlayoff($rootScope.squadraSelected.codice_squadra).then(function(data){
						$rootScope.showLoading();
						FantaCalcioServices.getSquadre($rootScope.user.idCliente, $rootScope.modoGioco).then(function(dataSquadre){
							dataSquadre.squadre.forEach(function(item){
								if(data.success.id_squadra == item.codice_squadra)
									$rootScope.squadraSelected = item;
							});
							$rootScope.hideLoading();
							navigator.notification.alert(data.success.msg_utente, function () { }, 'Iscrizione ai Playoff');
							$state.go('app.home', { forceReload: Math.random() * 50});
						},function(error){
							$rootScope.hideLoading();
							console.log("error getSquadre");
							console.log(error);
						});
					}, function(errorData){
			        	$rootScope.hideLoading();
						$rootScope.showToast(errorData.user_message);
					});
				}
			}, 'Iscrizione ai Playoff', ['Iscrivila', 'Non ora']);
		};


	});

  })();