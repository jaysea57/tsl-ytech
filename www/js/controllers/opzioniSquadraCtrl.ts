/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function () {
    var app = angular.module('opzioniSquadraCtrl', []);
    app.controller('opzioniSquadraCtrl', function ($scope, $rootScope, $state, $ionicHistory, $ionicLoading,
    	FantaCalcioServices: OM.FantaCalcioFactory) {

    	$scope.vm = {
    		nomeSquadra: '',
    		urlAvatar: '',
    		stemmi: []
    	};

    	function opzioniSquadraInit(){
            if(!$ionicHistory.forwardView()){
                $scope.vm.nomeSquadra = $rootScope.squadraSelected.nome;
                $rootScope.avatarTempImg = null;
            } else if($rootScope.nomeSquadraTemp){
                $scope.vm.nomeSquadra = $rootScope.nomeSquadraTemp;
            } else {
                $scope.vm.nomeSquadra = $rootScope.squadraSelected.nome;
                $rootScope.avatarTempImg = null;
            }
    		if($rootScope.avatarTempImg && $rootScope.avatarTempImg.nm_image){
    			$scope.vm.urlAvatar = 'http://' + $rootScope.avatarTempImg.nm_image;
            } else {
				$scope.vm.urlAvatar = 'http://' + $rootScope.squadraSelected.url_avatar_large;
            }
    		console.log($rootScope.squadraSelected);
            $rootScope.hideLoading();
    	};

        $scope.salvaModifiche = function(){
            $rootScope.showLoading();
            var avatarId;
            if($rootScope.avatarTempImg)
                avatarId = $rootScope.avatarTempImg.idAvatar;
            else
                avatarId = $rootScope.squadraSelected.idAvatar;
            FantaCalcioServices.modificaSquadra($rootScope.modoGioco, $rootScope.squadraSelected.codice_squadra, 
                $scope.vm.nomeSquadra, avatarId).then(function(data){
                    if(data.error){
                        $rootScope.showToast(data.error.user_message);
                    } else {
                        $rootScope.showToast(data.success.msg_utente);
                        FantaCalcioServices.getSquadre($rootScope.user.idCliente, $rootScope.modoGioco).then(function(dataSquadre){
                            dataSquadre.squadre.forEach(function(item){
                                if(data.success.squadra.codice_squadra == item.codice_squadra)
                                    $rootScope.squadraSelected = item;
                            });
                            if($rootScope.modoGioco == 3){
                                $rootScope.squadreTorneoClassico.forEach(function(item, index){
                                    if(item.codice_squadra == $rootScope.squadraSelected.codice_squadra){
                                        $rootScope.squadreTorneoClassico[index] = $rootScope.squadraSelected;
                                        if($rootScope.misterCalcioCup)
                                            window.localStorage.setItem('misterCalcioCupSquadreTorneoClassico'+$rootScope.user.idCliente,JSON.stringify($rootScope.squadreTorneoClassico));
                                        else
                                            window.localStorage.setItem('tuttoSportLeagueSquadreTorneoClassico'+$rootScope.user.idCliente,JSON.stringify($rootScope.squadreTorneoClassico));
                                    }
                                });
                            } else {
                                $rootScope.squadreTorneoGironi.forEach(function(item, index){
                                    if(item.codice_squadra == $rootScope.squadraSelected.codice_squadra){
                                        $rootScope.squadreTorneoGironi[index] = $rootScope.squadraSelected;
                                        if($rootScope.misterCalcioCup)
                                            window.localStorage.setItem('misterCalcioCupSquadreTorneoGironi'+$rootScope.user.idCliente,JSON.stringify($rootScope.squadreTorneoGironi));
                                        else
                                            window.localStorage.setItem('tuttoSportLeagueSquadreTorneoGironi'+$rootScope.user.idCliente,JSON.stringify($rootScope.squadreTorneoClassico));
                                    }
                                });
                            }     
                            //$rootScope.squadraSelected.nome = data.success.squadra.ds_squadra;
                            //$rootScope.squadraSelected.idAvatar = data.success.squadra.idAvatar;
                            if($rootScope.misterCalcioCup)
                                window.localStorage.setItem('misterCalcioCupSquadraSelected'+$rootScope.user.idCliente, JSON.stringify($rootScope.squadraSelected));
                            else
                                window.localStorage.setItem('tuttoSportLeagueSquadraSelected'+$rootScope.user.idCliente, JSON.stringify($rootScope.squadraSelected));
                            console.log(data);
                            $rootScope.hideLoading();
                        },function(dataSquadre){
                            $rootScope.hideLoading();
                            $rootScope.showToast('Errore nella connessione');
                        });
                    }
            },function(data){
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
            })
        };

    	$scope.modificaStemma = function(){
            $rootScope.showLoading();
            $rootScope.nomeSquadraTemp = $scope.vm.nomeSquadra;
    		$state.go('app.scegliStemma');
    	};
    	$scope.scegliStemmaInit = function(){
    		FantaCalcioServices.modificaAvatarSquadra($rootScope.modoGioco, $rootScope.squadraSelected.codice_squadra)
    		.then(function(data){
    			if(data.error){
                    $rootScope.showToast(data.error.user_message);
				} else {
					$scope.vm.stemmi = data.avatarList;
					console.log(data);
				}
                $rootScope.hideLoading();
			},function(data){
                $rootScope.hideLoading();
                $rootScope.showToast('Errore nella connessione');
			})
    	};
    	$scope.selezionaStemma = function(avatarImg){
            $rootScope.avatarTempImg = avatarImg;
    		//$rootScope.avatarTempImg = "data:image/png;base64," + avatarImg.bynaryImgLarge;
    		$ionicHistory.goBack();
    	};

        $scope.eliminaSquadra = function(){
            if(window.cordova){
                navigator.notification.confirm("Confermi l'eliminazione della squadra?", _eliminaSquadra, 
                    'Elimina squadra', ['Conferma', 'Annulla']);
            } else {
                if(confirm("Confermi l'eliminazione della squadra?")){
                    _eliminaSquadra(1);
                }
            }
        };

        function _eliminaSquadra(buttonIndex?: number){
            if(buttonIndex == 1){
                $rootScope.showLoading();
                FantaCalcioServices.eliminaSquadra($rootScope.squadraSelected.codice_squadra).then(function(data){
                    if(data.error){
                        $rootScope.showToast(data.error.user_message);
                    } else {
                        $rootScope.showToast(data.success.msg_utente);
                        $ionicHistory.nextViewOptions({
                            disableAnimate: true,
                            disableBack: true,
                            historyRoot: true
                        });
                        $rootScope.squadraSelected = null;
                        $state.go('app.home');
                    }
                    $rootScope.hideLoading();
                }, function(data){
                    $rootScope.hideLoading();
                    $rootScope.showToast('Errore nella connessione');
                });
            }
        };

        $scope.$on('$ionicView.beforeEnter', opzioniSquadraInit());

    });
})();
