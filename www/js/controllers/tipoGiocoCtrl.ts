/// <reference path="../../../typings/tsd.d.ts" />
/// <reference path="../model/model.ts" />
(function(){
	var app = angular.module('tipoGiocoCtrl', []);

	app.controller('tipoGiocoCtrl', function($scope, $rootScope, $state, $timeout, $ionicLoading, $ionicHistory,
	 FantaCalcioServices: OM.FantaCalcioFactory) {
	 	$scope.counterLogin = 0;
		$scope.goToLogin = function(modoGioco){
			//if($rootScope.misterCalcioCup)
			//	window.localStorage.setItem('misterCalcioCupModoGioco', modoGioco);
			//else
			//	window.localStorage.setItem('tuttoSportLeagueModoGioco', modoGioco);
			//$rootScope.modoGioco = modoGioco;

			if($rootScope.misterCalcioCup){
				if(modoGioco == 3){
					var loginData = JSON.parse(window.localStorage.getItem('misterCalcioCupUserLoginClassico'));
				} else {
					var loginData = JSON.parse(window.localStorage.getItem('misterCalcioCupUserLoginGironi'));
				}
			} else {
				if(modoGioco == 3){
					var loginData = JSON.parse(window.localStorage.getItem('tuttoSportLeagueUserLoginClassico'));
				} else {
					var loginData = JSON.parse(window.localStorage.getItem('tuttoSportLeagueUserLoginGironi'));
				}
			}
			if(loginData && loginData.email){
				doLogin(loginData, modoGioco);
			} else {
				$state.go('login', {gioco: modoGioco});
			}
		};
		if($rootScope.hideLoading){
			$rootScope.hideLoading();
		}

		function doLogin(datiLogin, modoGioco){
			if($scope.counterLogin <= 3){
				if($scope.counterLogin == 0)
					$rootScope.showLoading("Login in corso");
				$scope.counterLogin ++;
				FantaCalcioServices.authentication(datiLogin.email, datiLogin.password, modoGioco, $rootScope.deviceToken, $rootScope.deviceId, $rootScope.deviceTarget).then(function(data:OM.AuthenticationResponse){
					if(data.success){
						console.log(data);
						$rootScope.modoGioco = modoGioco;
						if($rootScope.misterCalcioCup){
							if($rootScope.modoGioco == 3){
								window.localStorage.setItem('misterCalcioCupUserTorneoClassico', JSON.stringify(data.success.user));
								$rootScope.userClassico = data.success.user;
							} else {
								window.localStorage.setItem('misterCalcioCupUserTorneoGironi', JSON.stringify(data.success.user));
								$rootScope.userGironi = data.success.user;
							}
						} else {
							if($rootScope.modoGioco == 3){
								window.localStorage.setItem('tuttoSportLeagueUserTorneoClassico', JSON.stringify(data.success.user));
								$rootScope.userClassico = data.success.user;
							} else {
								window.localStorage.setItem('tuttoSportLeagueUserTorneoGironi', JSON.stringify(data.success.user));
								$rootScope.userGironi = data.success.user;
							}
						}
						$rootScope.user = data.success.user;
						$ionicHistory.nextViewOptions({
							disableAnimate: true,
							disableBack: true,
							historyRoot: true
						});
						if (window.cordova) {
							AdMob.showInterstitial(function () {
								AdMob.prepareInterstitial({
									adId: $rootScope.admobid.interstitial,
									autoShow: false
								});
								$rootScope.showBanner();
								$state.go('app.home');
							}, function () {
								AdMob.prepareInterstitial({
									adId: $rootScope.admobid.interstitial,
									autoShow: false
								});
								$rootScope.showBanner();
								$state.go('app.home');
							});							
						}
						else {
							console.log("going app.home from tipogioco");
							$rootScope.hideLoading();
							$state.go('app.home');
						}
					} else if (data.error){
						console.log(data);
						doLogin(datiLogin, modoGioco);
						if ($scope.counterLogin > 3) {
							if($rootScope.misterCalcioCup){
								window.localStorage.removeItem('misterCalcioCupUserLogin');
								window.localStorage.removeItem('misterCalcioCupUserLoginClassico');
								window.localStorage.removeItem('misterCalcioCupUserLoginGironi');
							} else {
								window.localStorage.removeItem('tuttoSportLeagueUserLogin');
								window.localStorage.removeItem('tuttoSportLeagueUserLoginClassico');
								window.localStorage.removeItem('tuttoSportLeagueUserLoginGironi');
							}
							$state.go('login', {gioco: modoGioco});
							//$rootScope.hideLoading();
						}
					}
				},function(data){
					$rootScope.hideLoading();
					$rootScope.showToast('Errore nella connessione');
				});
			}
		};

	});
})();