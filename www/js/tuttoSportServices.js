/// <reference path="../../typings/tsd.d.ts" />
/// <reference path="model/model.ts" />
(function () {
    var app = angular.module('Services', []);
    app.factory('FantaCalcioServices', ['$q', '$http', '$rootScope', function ($q, $http, $rootScope) {
            $rootScope.torneoClassicoUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
                ? 'http://localhost:9292/league.tsport.yland.it/'
                : 'http://league.tuttosport.com/'; //PRODUZIONE 
            //:'http://devts.tuttosport.com/';       						//RETE AZIENDALE Y TECH
            //MISTER CALCIO CUP CLASSICO
            //?'http://localhost:9292/gp.mr.yland.it/'
            //:'http://gp.mr.yland.it/'									//RETE ESTERNA
            //:'http://devgp.corrieredellosport.it/'                       	//RETE AZIENDALE Y TECH
            ;
            $rootScope.torneoGironiUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
                ? 'http://localhost:9292/gironi.tsport.yland.it/'
                : 'http://gironi-league.tuttosport.com/'; //PRODUZIONE
            //:'http://devtsgironi.tuttosport.com/';       				//RETE AZIENDALE Y TECH
            //MISTER CALCIO CUP GIRONI modo_gioco = 1
            //?'http://localhost:9292/www.mr.yland.it/'
            //:'http://www.mr.yland.it/';								//ESTERNA
            //:'http://devmcc.corrieredellosport.it/'                       	//RETE AZIENDALE Y TECH
            ;
            $rootScope.torneoLegheUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
                ? 'http://localhost:9292/gironi.tsport.yland.it/'
                : 'http://gironi-league.tuttosport.com/'; //PRODUZIONE
            //:'http://devtsgironi.tuttosport.com/';       				//RETE AZIENDALE Y TECH
            //MISTER CALCIO CUP LEGHE modo_gioco = 1  
            //?'http://localhost:9292/www.mr.yland.it/'
            //:'http://www.mr.yland.it/';								//ESTERNA
            //:'http://devmcc.corrieredellosport.it/'					//RETE AZIENDALE Y TECH
            ;
            $rootScope.torneoPlayoffUrl = window.location.host.indexOf('192') == 0 || window.location.host.indexOf('localhost') == 0
                ? 'http://localhost:9292/gironi.tsport.yland.it/'
                : 'http://gironi-league.tuttosport.com/'; //PRODUZIONE
            //:'http://devtsgironi.tuttosport.com/';       				//RETE AZIENDALE Y TECH
            //MISTER CALCIO CUP PLAYOFF modo_gioco = 1
            //?'http://localhost:9292/www.mr.yland.it/'
            //:'http://www.mr.yland.it/';								//ESTERNA
            //:'http://devmcc.corrieredellosport.it/'					//RETE AZIENDALE Y TECH
            ;
            return {
                authentication: function (username, password, modoGioco, deviceToken, deviceId, deviceTarget) {
                    var urlServizi;
                    //var modo = parseInt($rootScope.modoGioco);
                    switch (parseInt(modoGioco)) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var os;
                    if (typeof deviceTarget == 'undefined') {
                        os = 'browser';
                        deviceId = null;
                        deviceTarget = null;
                        deviceToken = null;
                    }
                    else {
                        os = deviceTarget.toLowerCase();
                        if (os == 'apple') {
                            os = 'ios';
                        }
                    }
                    console.log("authentication urlServizi: " + urlServizi
                        + " username: " + username
                        + " deviceId: " + deviceId
                        + " deviceToken: " + deviceToken
                        + " deviceTarget: " + deviceTarget
                        + " os: " + os);
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/authentication.json', { params: {
                            os: 'ios',
                            appversion: '1.0',
                            tipologin: 'sso',
                            password: password,
                            action: 'login',
                            modo_gioco: modoGioco,
                            username: username,
                            deviceId: deviceId,
                            deviceToken: deviceToken,
                            deviceTarget: deviceTarget
                        } }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                facebookAuthentication: function (username, password, modoGioco, token, deviceToken, deviceId, deviceTarget) {
                    // /authentication.json?action=login&appversion=1.3.4&modo_gioco=3&os=ios&password=ardizzone&tipologin=sso&username=lorenzo.ardizzone@gmail.com
                    //facebookAuthentication: function(modoGioco: any, token: string){ // /authentication.json?action=login&appversion=1.3.4&modo_gioco=3&os=ios&password=ardizzone&tipologin=sso&username=lorenzo.ardizzone@gmail.com
                    var urlServizi;
                    //var modo = parseInt($rootScope.modoGioco);
                    switch (parseInt(modoGioco)) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var os;
                    if (typeof deviceTarget == 'undefined') {
                        os = 'browser';
                        deviceId = null;
                        deviceTarget = null;
                        deviceToken = null;
                    }
                    else {
                        os = deviceTarget.toLowerCase();
                        if (os == 'apple') {
                            os = 'ios';
                        }
                    }
                    console.log("fb authentication urlServizi: " + urlServizi
                        + " username: " + username
                        + " deviceId: " + deviceId
                        + " deviceToken: " + deviceToken
                        + " deviceTarget: " + deviceTarget
                        + " os: " + os);
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/authentication.json', { params: {
                            os: 'ios',
                            appversion: '1.0',
                            tipologin: 'facebook',
                            password: password,
                            action: 'login',
                            token: token,
                            modo_gioco: modoGioco,
                            username: username,
                            deviceId: deviceId,
                            deviceToken: deviceToken,
                            deviceTarget: deviceTarget
                        } }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                logout: function (idCliente, modoGioco) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/login.json', {
                        params: {
                            action: 'logout',
                            ic: idCliente,
                            modo_gioco: modoGioco,
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                registraAPlayoff: function (idSquadra) {
                    var urlServizi;
                    //var modo = parseInt($rootScope.modoGioco);
                    switch (parseInt($rootScope.modoGioco)) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + '/mobile/playoff.json', {
                        params: {
                            is: idSquadra,
                            action: 'IscriviSquadra'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                getSquadre: function (idCliente, modoGioco) {
                    var urlServizi;
                    //var modo = parseInt($rootScope.modoGioco);
                    switch (parseInt(modoGioco)) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/home.json', {
                        params: {
                            ic: idCliente,
                            action: 'getSquadre',
                            modo_gioco: modoGioco
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                getCalendario: function (idSquadra) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/Calendario.json', {
                        params: {
                            is: idSquadra,
                            action: 'getCalendario'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                getDettaglioGiornata: function (idSquadra, idPartita) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/tabellino.json', {
                        params: {
                            ip: idPartita,
                            is: idSquadra,
                            action: 'getDettaglio'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                getClassifica: function (idSquadra) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/classifica.json', {
                        params: {
                            is: idSquadra,
                            action: 'getClassifica'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                modificaAvatarSquadra: function (idCampionato, idSquadra) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer(); //stemma.json?action=getStemmi  ????
                    $http.get(urlServizi + 'mobile/stemma.json', {
                        params: {
                            action: 'getStemmi',
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                modificaSquadra: function (idCampionato, idSquadra, nomeSquadra, idAvatar) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/squadra.json', {
                        params: {
                            team: nomeSquadra,
                            idavatar: idAvatar,
                            action: 'modificaSquadra',
                            is: idSquadra
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                eliminaSquadra: function (idSquadra) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/squadra.json', {
                        params: {
                            action: 'cancellaSquadra',
                            is: idSquadra
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                nuovaSquadra: function (idCliente, modoGioco) {
                    var urlServizi;
                    //var modo = parseInt($rootScope.modoGioco);
                    switch (parseInt(modoGioco)) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/squadra.json', {
                        params: {
                            action: 'addSquadra',
                            idc: idCliente,
                            operation: 'add',
                            modo_gioco: modoGioco,
                            avatarlist: false
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                gestioneRosa: function (idCampionato, idSquadra) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'scripts/rosadata.jsp', {
                        params: {
                            is: idSquadra,
                            ic: idCampionato,
                            format: 'json'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                modificaRosa: function (idSquadra, sold, bought) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var date = new Date();
                    var rand = date.getTime();
                    var deferred = $q.defer();
                    var append = '';
                    if (modo == 3)
                        append = 'gprixteam/create_rosa_mobile.jsp';
                    else
                        append = 'squadra/create_rosa_mobile.jsp';
                    $http.get(urlServizi + append, {
                        params: {
                            rand: rand,
                            sold: sold,
                            bought: bought,
                            is: idSquadra,
                            format: 'json'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                formazioneData: function (idCampionato, idSquadra) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'scripts/formazionedata.jsp', {
                        params: {
                            is: idSquadra,
                            ic: idCampionato,
                            format: 'json'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                salvaFormazione: function (idCampionato, idSquadra, tattica, formazione) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var date = new Date();
                    var rand = date.getTime();
                    var deferred = $q.defer();
                    var urlChiamata = '';
                    if (modo == 3) {
                        urlChiamata = 'gprixteam/create_formazione_mobile.jsp';
                    }
                    else {
                        urlChiamata = 'squadra/create_formazione_mobile.jsp';
                    }
                    $http.get(urlServizi + urlChiamata, {
                        params: {
                            rand: rand,
                            tattica: tattica,
                            ic: idCampionato,
                            add: formazione,
                            is: idSquadra,
                            format: 'json'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                modificaFormazione: function (idCampionato, idSquadra, tattica, remove, add) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var date = new Date();
                    var rand = date.getTime();
                    var deferred = $q.defer();
                    var urlChiamata = '';
                    if (modo == 3) {
                        urlChiamata = 'gprixteam/create_formazione_mobile.jsp';
                    }
                    else {
                        urlChiamata = 'squadra/create_formazione_mobile.jsp';
                    }
                    $http.get(urlServizi + urlChiamata, {
                        params: {
                            ic: idCampionato,
                            is: idSquadra,
                            tattica: tattica,
                            remove: remove,
                            add: add,
                            rand: rand
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                cancellaFormazione: function (idCampionato, idSquadra, tattica) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var date = new Date();
                    var rand = date.getTime();
                    var deferred = $q.defer();
                    var urlChiamata = '';
                    if (modo == 3) {
                        urlChiamata = 'gprixteam/create_formazione_mobile.jsp';
                    }
                    else {
                        urlChiamata = 'squadra/create_formazione_mobile.jsp';
                    }
                    $http.get(urlServizi + urlChiamata, {
                        params: {
                            rand: rand,
                            tattica: tattica,
                            ic: idCampionato,
                            clrfrmzn: 'yes',
                            is: idSquadra,
                            format: 'json'
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                getNews: function () {
                    var urlServizi;
                    urlServizi = $rootScope.torneoClassicoUrl;
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/news.json', {
                        params: {
                            action: "getNews"
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                registraUtente: function (email, password, nome, cognome, username, dataNascita, regolamento, fl_privacy, fl_mktg, fl_servizio_mcc, fl_servizio_cds, fl_articoli_privacy, modoGioco) {
                    var urlServizi;
                    //var modo = parseInt($rootScope.modoGioco);
                    switch (parseInt(modoGioco)) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/register.json', {
                        params: {
                            action: 'addUser',
                            MAIL: email,
                            PASSWORD: password,
                            NOME: nome,
                            COGNOME: cognome,
                            USERNAME: username,
                            DTNASCITA: dataNascita,
                            Regolamento: regolamento,
                            fl_privacy: fl_privacy,
                            fl_mktg: fl_mktg,
                            fl_servizio_mcc: fl_servizio_mcc,
                            fl_servizio_cds: fl_servizio_cds,
                            fl_articoli_privacy: fl_articoli_privacy,
                            modo_gioco: modoGioco
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                registraUtenteFB: function (email, token, regolamento, fl_privacy, fl_mktg, fl_servizio_mcc, fl_servizio_cds, fl_articoli_privacy, modoGioco) {
                    var urlServizi;
                    //var modo = parseInt($rootScope.modoGioco);
                    switch (parseInt(modoGioco)) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/registerFB.json', {
                        params: {
                            email: email,
                            action: 'addUser',
                            Regolamento: regolamento,
                            fl_privacy: fl_privacy,
                            fl_mktg: fl_mktg,
                            fl_servizio_mcc: fl_servizio_mcc,
                            fl_servizio_cds: fl_servizio_cds,
                            fl_articoli_privacy: fl_articoli_privacy,
                            modo_gioco: modoGioco,
                            token: token
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                getProfilo: function (idCliente) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/user.json', {
                        params: {
                            action: 'datiProfilo',
                            idc: idCliente
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                modificaProfilo: function (idCliente, NOME, COGNOME, DTNASCITA, SESSO, MAIL, INDIR, CITTA, PROV, CAP, UNSUB, MAIL_2, INDIRIZZO_SPED, CAP_SPED, CITTA_SPED, PROV_SPED) {
                    var urlServizi;
                    var modo = parseInt($rootScope.modoGioco);
                    switch (modo) {
                        case 3:
                            urlServizi = $rootScope.torneoClassicoUrl;
                            break;
                        case 1:
                            urlServizi = $rootScope.torneoGironiUrl;
                            break;
                        case 2:
                            urlServizi = $rootScope.torneoLegheUrl;
                            break;
                        case 4:
                            urlServizi = $rootScope.torneoPlayoffUrl;
                            break;
                    }
                    var deferred = $q.defer();
                    $http.get(urlServizi + 'mobile/user.json', {
                        params: {
                            action: 'updateUser',
                            idc: idCliente,
                            NOME: NOME,
                            COGNOME: COGNOME,
                            DTNASCITA: DTNASCITA,
                            SESSO: SESSO,
                            MAIL: MAIL,
                            INDIR: INDIR,
                            CITTA: CITTA,
                            PROV: PROV,
                            CAP: CAP,
                            UNSUB: UNSUB,
                            MAIL_2: MAIL_2,
                            INDIRIZZO_SPED: INDIRIZZO_SPED,
                            CAP_SPED: CAP_SPED,
                            CITTA_SPED: CITTA_SPED,
                            PROV_SPED: PROV_SPED
                        }
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                }
            };
        }]);
    app.service('RosaService', function ($rootScope) {
        var offerte = false;
        var primaRosa = false;
        var bloccoRosa = false;
        var portieriRosa = [];
        var difensoriRosa = [];
        var centrocampistiRosa = [];
        var attaccantiRosa = [];
        var allenatoriRosa = [];
        var giocatoriDisponibili = [];
        var giocatoreSelezionato = null;
        var giocatoriVenduti = '';
        var giocatoriAcquistati = '';
        var saldo = 0;
        var lastSelected = null;
        return {
            setBloccoRosa: function (value) {
                bloccoRosa = value;
            },
            getBloccoRosa: function () {
                return bloccoRosa;
            },
            setOfferte: function (value) {
                offerte = value;
            },
            getOfferte: function () {
                return offerte;
            },
            setPrimaRosa: function (value) {
                primaRosa = value;
            },
            getPrimaRosa: function () {
                return primaRosa;
            },
            setGiocatore: function (giocatore, offerta) {
                switch (lastSelected.ruolo) {
                    case 'P':
                        var giocatoreRosaCast = {};
                        angular.extend(giocatoreRosaCast, giocatore);
                        giocatoreRosaCast.COSTO_ACQUISTO = offerta;
                        giocatoreRosaCast.IN_FORMAZIONE = false;
                        portieriRosa[lastSelected.index] = giocatoreRosaCast;
                        break;
                    case 'D':
                        var giocatoreRosaCast = {};
                        angular.extend(giocatoreRosaCast, giocatore);
                        giocatoreRosaCast.COSTO_ACQUISTO = offerta;
                        giocatoreRosaCast.IN_FORMAZIONE = false;
                        difensoriRosa[lastSelected.index] = giocatoreRosaCast;
                        break;
                    case 'C':
                        var giocatoreRosaCast = {};
                        angular.extend(giocatoreRosaCast, giocatore);
                        giocatoreRosaCast.COSTO_ACQUISTO = offerta;
                        giocatoreRosaCast.IN_FORMAZIONE = false;
                        centrocampistiRosa[lastSelected.index] = giocatoreRosaCast;
                        break;
                    case 'A':
                        var giocatoreRosaCast = {};
                        angular.extend(giocatoreRosaCast, giocatore);
                        giocatoreRosaCast.COSTO_ACQUISTO = offerta;
                        giocatoreRosaCast.IN_FORMAZIONE = false;
                        attaccantiRosa[lastSelected.index] = giocatoreRosaCast;
                        break;
                    case 'AL':
                        var giocatoreRosaCast = {};
                        angular.extend(giocatoreRosaCast, giocatore);
                        giocatoreRosaCast.COSTO_ACQUISTO = offerta;
                        giocatoreRosaCast.IN_FORMAZIONE = false;
                        allenatoriRosa[lastSelected.index] = giocatoreRosaCast;
                        break;
                }
                giocatoriDisponibili.splice(giocatoriDisponibili.indexOf(giocatore), 1);
            },
            giocatoreRemovedFromRosa: function (giocatore) {
                var rosaGiocatoreCast = {};
                angular.extend(rosaGiocatoreCast, giocatore);
                //rosaGiocatoreCast.VALORE_ATTUALE = giocatore.COSTO_ACQUISTO;
                delete rosaGiocatoreCast.COSTO_ACQUISTO;
                delete rosaGiocatoreCast.IN_FORMAZIONE;
                giocatoriDisponibili.push(rosaGiocatoreCast);
            },
            getPortieri: function (sortBy, crescente, squadra, costoMin, costoMax) {
                var giocatori = [];
                if (giocatoriDisponibili.length > 0) {
                    giocatoriDisponibili.forEach(function (item) {
                        if (item.RUOLO == 0 && (squadra.length == 0 || squadra.indexOf(item.SQUADRA) > -1) &&
                            (item.VALORE_ATTUALE >= costoMin && item.VALORE_ATTUALE <= costoMax)) {
                            giocatori.push(item);
                        }
                    });
                }
                if (sortBy.toLowerCase() == "cognome") {
                    if (crescente) {
                        giocatori.sort(compareByCognomeCrescente);
                    }
                    else {
                        giocatori.sort(compareByCognomeDecrescente);
                    }
                }
                else if (sortBy.toLowerCase() == "costo") {
                    if (crescente) {
                        giocatori.sort(compareByCostoCrescente);
                    }
                    else {
                        giocatori.sort(compareByCostoDecrescente);
                    }
                }
                else {
                    return [];
                }
                return giocatori;
            },
            getDifensori: function (sortBy, crescente, squadra, costoMin, costoMax) {
                var giocatori = [];
                if (giocatoriDisponibili.length > 0) {
                    giocatoriDisponibili.forEach(function (item) {
                        if (item.RUOLO == 1 && (squadra.length == 0 || squadra.indexOf(item.SQUADRA) > -1) &&
                            (item.VALORE_ATTUALE >= costoMin && item.VALORE_ATTUALE <= costoMax)) {
                            giocatori.push(item);
                        }
                    });
                }
                if (sortBy.toLowerCase() == "cognome") {
                    if (crescente) {
                        giocatori.sort(compareByCognomeCrescente);
                    }
                    else {
                        giocatori.sort(compareByCognomeDecrescente);
                    }
                }
                else if (sortBy.toLowerCase() == "costo") {
                    if (crescente) {
                        giocatori.sort(compareByCostoCrescente);
                    }
                    else {
                        giocatori.sort(compareByCostoDecrescente);
                    }
                }
                else {
                    return [];
                }
                return giocatori;
            },
            getCentrocampisti: function (sortBy, crescente, squadra, costoMin, costoMax) {
                var giocatori = [];
                if (giocatoriDisponibili.length > 0) {
                    giocatoriDisponibili.forEach(function (item) {
                        if (item.RUOLO == 2 && (squadra.length == 0 || squadra.indexOf(item.SQUADRA) > -1) &&
                            (item.VALORE_ATTUALE >= costoMin && item.VALORE_ATTUALE <= costoMax)) {
                            giocatori.push(item);
                        }
                    });
                }
                if (sortBy.toLowerCase() == "cognome") {
                    if (crescente) {
                        giocatori.sort(compareByCognomeCrescente);
                    }
                    else {
                        giocatori.sort(compareByCognomeDecrescente);
                    }
                }
                else if (sortBy.toLowerCase() == "costo") {
                    if (crescente) {
                        giocatori.sort(compareByCostoCrescente);
                    }
                    else {
                        giocatori.sort(compareByCostoDecrescente);
                    }
                }
                else {
                    return [];
                }
                return giocatori;
            },
            getAttaccanti: function (sortBy, crescente, squadra, costoMin, costoMax) {
                var giocatori = [];
                if (giocatoriDisponibili.length > 0) {
                    giocatoriDisponibili.forEach(function (item) {
                        if (item.RUOLO == 3 && (squadra.length == 0 || squadra.indexOf(item.SQUADRA) > -1) &&
                            (item.VALORE_ATTUALE >= costoMin && item.VALORE_ATTUALE <= costoMax)) {
                            giocatori.push(item);
                        }
                    });
                }
                if (sortBy.toLowerCase() == "cognome") {
                    if (crescente) {
                        giocatori.sort(compareByCognomeCrescente);
                    }
                    else {
                        giocatori.sort(compareByCognomeDecrescente);
                    }
                }
                else if (sortBy.toLowerCase() == "costo") {
                    if (crescente) {
                        giocatori.sort(compareByCostoCrescente);
                    }
                    else {
                        giocatori.sort(compareByCostoDecrescente);
                    }
                }
                else {
                    return [];
                }
                return giocatori;
            },
            getAllenatori: function (sortBy, crescente, squadra, costoMin, costoMax) {
                var giocatori = [];
                if (giocatoriDisponibili.length > 0) {
                    giocatoriDisponibili.forEach(function (item) {
                        if (item.RUOLO == 4 && (squadra.length == 0 || squadra.indexOf(item.SQUADRA) > -1) &&
                            (item.VALORE_ATTUALE >= costoMin && item.VALORE_ATTUALE <= costoMax)) {
                            giocatori.push(item);
                        }
                    });
                }
                if (sortBy.toLowerCase() == "cognome") {
                    if (crescente) {
                        giocatori.sort(compareByCognomeCrescente);
                    }
                    else {
                        giocatori.sort(compareByCognomeDecrescente);
                    }
                }
                else if (sortBy.toLowerCase() == "costo") {
                    if (crescente) {
                        giocatori.sort(compareByCostoCrescente);
                    }
                    else {
                        giocatori.sort(compareByCostoDecrescente);
                    }
                }
                else {
                    return [];
                }
                return giocatori;
            },
            setRosa: function (portieri, difensori, centrocampisti, attaccanti, allenatori) {
                portieriRosa = portieri;
                difensoriRosa = difensori;
                centrocampistiRosa = centrocampisti;
                attaccantiRosa = attaccanti;
                if ($rootScope.misterCalcioCup)
                    allenatoriRosa = allenatori;
            },
            getRosa: function () {
                if (!$rootScope.mistercalciocup)
                    return portieriRosa.concat(difensoriRosa, centrocampistiRosa, attaccantiRosa, allenatoriRosa);
                else
                    ($rootScope.mistercalciocup);
                return portieriRosa.concat(difensoriRosa, centrocampistiRosa, attaccantiRosa, allenatoriRosa, allenatoriRosa);
            },
            getSquadre: function () {
                var elencoSquadre = [];
                giocatoriDisponibili.forEach(function (item) {
                    if (elencoSquadre.indexOf(item.SQUADRA) == -1) {
                        elencoSquadre.push(item.SQUADRA);
                    }
                });
                return elencoSquadre;
            },
            setGiocatoriDisponibili: function (_giocatoriDisponibili) {
                giocatoriDisponibili = _giocatoriDisponibili;
            },
            getGiocatoriDisponibili: function () {
                return giocatoriDisponibili;
            },
            setGiocatoreSelezionato: function (_giocatoreSelezionato) {
                giocatoreSelezionato = _giocatoreSelezionato;
            },
            getGiocatoreSelezionato: function () {
                return giocatoreSelezionato;
            },
            setGiocatoriVenduti: function (_giocatoriVenduti) {
                giocatoriVenduti = _giocatoriVenduti;
            },
            getGiocatoriVenduti: function () {
                return giocatoriVenduti;
            },
            setGiocatoriAcquistati: function (_giocatoriAcquistati) {
                giocatoriAcquistati = _giocatoriAcquistati;
            },
            getGiocatoriAcquistati: function () {
                return giocatoriAcquistati;
            },
            setLastSelected: function (_lastSelected) {
                lastSelected = _lastSelected;
            },
            getLastSelected: function () {
                return lastSelected;
            },
            setSaldo: function (_saldo) {
                saldo = _saldo;
            },
            getSaldo: function () {
                return saldo;
            }
        };
        //FUNZIONI PER IL SORTING
        function compareByCognomeCrescente(a, b) {
            if (a.COGNOME < b.COGNOME)
                return -1;
            if (a.COGNOME > b.COGNOME)
                return 1;
            return 0;
        }
        ;
        function compareByCostoCrescente(a, b) {
            if (a.VALORE_ATTUALE < b.VALORE_ATTUALE)
                return -1;
            if (a.VALORE_ATTUALE > b.VALORE_ATTUALE)
                return 1;
            return 0;
        }
        ;
        function compareByCognomeDecrescente(a, b) {
            if (a.COGNOME > b.COGNOME)
                return -1;
            if (a.COGNOME < b.COGNOME)
                return 1;
            return 0;
        }
        ;
        function compareByCostoDecrescente(a, b) {
            if (a.VALORE_ATTUALE > b.VALORE_ATTUALE)
                return -1;
            if (a.VALORE_ATTUALE < b.VALORE_ATTUALE)
                return 1;
            return 0;
        }
        ;
    });
})();
//# sourceMappingURL=tuttoSportServices.js.map